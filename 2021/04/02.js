const fs = require('fs')

function transpose(m) {
    return m[0].map((x, i) => m.map(x => x[i]))
}

function sumOfUnmarked(board) {
    return board.packets
        .flat()
        .filter(it => !board.marked.has(it))
        .map(it => +it)
        .reduce((acc, e) => acc + e, 0) / 2 // div 2 because each element is counted twice (rows and columns)
}

function wins(board) {
    return board.packets
        .some(packet => packet.every(it => board.marked.has(it)))
}

function readBoard(input) {
    input.shift()
    const rows = []
    rows.push(input.shift().split(' '))
    rows.push(input.shift().split(' '))
    rows.push(input.shift().split(' '))
    rows.push(input.shift().split(' '))
    rows.push(input.shift().split(' '))
    return {
        packets: rows.concat(transpose(rows)),
        marked: new Set()
    }
}

function readBoards(input) {
    const boards = []
    while (input.length >= 6) {
        boards.push(readBoard(input))
    }
    return boards
}

function play(boards, number) {
    boards.forEach(b => b.marked.add(number))
}

function winingBoards(boards) {
    return boards.filter(it => wins(it))
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    const numbers = input.shift().split(',')
    let boards = readBoards(input)
    do {
        var number = numbers.shift()
        play(boards, number)
        var boardsThatWonThisTurn = winingBoards(boards)
        boards = boards.filter(it => !boardsThatWonThisTurn.includes(it))

    } while (boards.length > 0)
    console.log(1 * number * sumOfUnmarked(boardsThatWonThisTurn[0]))
})