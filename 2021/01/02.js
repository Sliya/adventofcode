const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n').map(it => +it)
    console.log(input.map((elt, idx, arr) => idx >= arr.length - 3 ? arr[arr.length - 1] + arr[arr.length - 2] + arr[arr.length - 3] : elt + arr[idx + 1] + arr[idx + 2])
        .map((elt, idx, arr) => idx == arr.length - 1 ? 0 : arr[idx + 1] - elt)
        .filter(it => it > 0)
        .length)
})
