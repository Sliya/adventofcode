const fs = require('fs')

function combine(a1, a2) {
    return a1.map((line, y) => line.map((cell, x) => cell || a2[y][x]))
}

function fold(grid, instruction) {
    if (instruction[0] == 'y') {
        return combine(grid.slice(0, instruction[1]), grid.slice(instruction[1] + (grid.length % 2)).reverse())

    } else {
        return combine(grid.map(line => line.slice(0, instruction[1])), grid.map(line => line.slice(instruction[1] + (line.length % 2)).reverse()))
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    const grid = input.filter(it => it.includes(',')).map(line => line.split(',').map(it => +it))
    const instructions = input.filter(it => it.includes('=')).map(it => [it.charAt(11), +it.substring(13)])
    const maxX = grid.map(it => it[0]).reduce((a, b) => a > b ? a : b) + 1
    const maxY = grid.map(it => it[1]).reduce((a, b) => a > b ? a : b) + 1
    const paper = Array(maxY).fill().map(it => Array(maxX).fill(false))
    grid.forEach(it => paper[it[1]][it[0]] = true)
    console.log(fold(paper, instructions[0]).flat().filter(it => it).length)
})