const fs = require('fs')


function fold(grid, instruction) {
    const ret = []
    if (instruction[0] == 'y') {
        for (let i = 0; i < instruction[1]; i++) {
            ret.push([])
            for (let j = 0; j < grid[i].length; j++) {
                ret[i].push(grid[i][j] || grid[grid.length - 1 - i][j])
            }
        }
    } else {
        for (let i = 0; i < grid.length; i++) {
            ret.push([])
            for (let j = 0; j < instruction[1]; j++) {
                ret[i].push(grid[i][j] || grid[i][grid[i].length - 1 - j])
            }
        }
    }
    return ret
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    const grid = input.filter(it => it.includes(',')).map(line => line.split(',').map(it => +it))
    const instructions = input.filter(it => it.includes('=')).map(it => [it.charAt(11), +it.substring(13)])

    const maxX = grid.map(it => it[0]).reduce((a, b) => a > b ? a : b) + 1
    const maxY = Math.max(...grid.map(it => it[1])) + 2 // Fucking +2 instead of +1, I don't even know why... FML

    let paper = Array(maxY).fill().map(_ => Array(maxX).fill(false))
    grid.forEach(([x, y]) => paper[y][x] = true)
    instructions.forEach(instruction => {
        paper = fold(paper, instruction)
    })
    console.log(paper.map(it => it.map(that => that ? '#' : ' ').join('')).join('\n'))
})
