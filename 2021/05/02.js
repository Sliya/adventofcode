const fs = require('fs')

const grid = {}

function sign(a) {
    return a < 0 ? -1 : a > 0 ? 1 : 0
}

function trace(p1, p2) {
    x = p1.x
    y = p1.y
    do {
        const coords = x + ',' + y
        if (grid[coords] == undefined) {
            grid[coords] = 0
        }
        grid[coords]++
        x += sign(p2.x - p1.x)
        y += sign(p2.y - p1.y)
    } while (x != p2.x + (sign(p2.x - p1.x)) || y != p2.y + (sign(p2.y - p1.y)))
}

fs.readFile('input', 'utf8', (error, data) => {
    data.trim().split('\n')
        .map(it => it.split(' -> ')
            .map(t => t.split(',').map(i => +i))
            .map(t => {
                return {
                    x: t[0],
                    y: t[1]
                }
            }))
        .forEach(it => trace(it[0], it[1]))
    let res = 0
    for (const key in grid) {
        if (grid[key] > 1)
            res++
    }
    console.log(res)

})