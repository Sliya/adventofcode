const fs = require('fs')

function age(squids) {
    const ret = squids.map((elt, idx) => squids[(idx + 1) % 9])
    ret[8] = squids[0]
    ret[6] += squids[0]
    return ret;
}

fs.readFile('input', 'utf8', (error, data) => {
    let squids = Array(9).fill(0)
    data.trim().split(',').map(it => +it).forEach(s => squids[s]++)
    for (let i = 0; i < 80; i++) {
        squids = age(squids)
    }
    console.log(squids.reduce((a, b) => a + b))
})