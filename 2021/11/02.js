const fs = require('fs')

function isInside(x, y) {
    return x >= 0 && x < 10 && y >= 0 && y < 10
}

function flash(grid, x, y) {
    for (let i = y - 1; i <= y + 1; i++) {
        for (let j = x - 1; j <= x + 1; j++) {
            if (!isInside(j, i)) continue
            grid[i][j]++
        }
    }
}

function inc(grid) {
    for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
            grid[i][j]++
        }
    }
}

function reset(grid) {
    for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
            grid[i][j] = grid[i][j] > 9 ? 0 : grid[i][j]
        }
    }
}

function step(grid) {
    inc(grid)
    const hasFlashed = grid.map(l => l.map(_ => false))
    let nbFlashes = 0
    do {
        var hasFlashedThisTurn = false
        for (let i = 0; i < 10; i++) {
            for (let j = 0; j < 10; j++) {
                if (!hasFlashed[i][j] && grid[i][j] > 9) {
                    nbFlashes++
                    flash(grid, j, i)
                    hasFlashed[i][j] = true
                    hasFlashedThisTurn = true
                }
            }
        }
    } while (hasFlashedThisTurn)
    reset(grid)
    return nbFlashes
}

fs.readFile('input', 'utf8', (error, data) => {
    const grid = data.trim().split('\n').map(it => it.split('').map(it => +it))
    let i = 1
    while (step(grid) < 100) {
        i++
    }
    console.log(i)
})