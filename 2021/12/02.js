const fs = require('fs')

const graph = {}

function isBig(cave) {
    return (cave[0] >= 'A' && cave[0] <= 'Z') || cave == 'start' || cave == 'end'
}

function addToGraph(pair) {
    if (graph[pair[0]] == undefined) {
        graph[pair[0]] = []
    }
    if (graph[pair[1]] == undefined) {
        graph[pair[1]] = []
    }
    if (pair[1] != 'start') {
        graph[pair[0]].push(pair[1])
    }
    if (pair[0] != 'start') {
        graph[pair[1]].push(pair[0])
    }
}


function canVisit(path, cave) {
    return isBig(cave) || !path.includes(cave) || (path.filter(it => !isBig(it)).length - [...new Set(path.filter(it => !isBig(it)))].length < 1 && path.filter(it => it == cave).length < 2)
}


function bfs() {
    const paths = []
    const unfinishedPaths = [
        ['start']
    ]
    while (unfinishedPaths.length > 0) {
        const path = unfinishedPaths.pop()
        for (neighbor of graph[path[path.length - 1]]) {
            if (neighbor == 'end') {
                paths.push(path.concat([neighbor]))
                continue
            } else if (canVisit(path, neighbor)) {
                unfinishedPaths.push(path.concat([neighbor]))
                continue
            }
        }
    }
    return paths;
}

fs.readFile('input', 'utf8', (error, data) => {
    data.trim().split('\n').map(it => it.split('-')).forEach(addToGraph)
    console.log(bfs().length)
})