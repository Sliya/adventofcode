const fs = require('fs')
const _ = require('lodash')
const map = fs.readFileSync('input', 'utf8').trim().split('\n').map(it => it.split('').map(that => +that))
const sommets = map.map((l, y) => l.map((weight, x) => {
    return {
        x,
        y,
        h: x + ',' + y
    }
})).flat()

const prec = {}
const length = {}
length['0,0'] = 0

function isInside(y, x) {
    return y >= 0 && y < map.length && x >= 0 && x < map[0].length
}

function neighbors(x, y) {
    const ret = []
    for (let i = y - 1; i <= y + 1; i++) {
        for (let j = x - 1; j <= x + 1; j++) {
            if (!isInside(i, j) || (i != y && x != j) || (i == y && j == x)) continue
            const n = {
                x: j,
                y: i
            }
            n.h = hash(n)
            ret.push(n)
        }
    }
    return ret
}

function hash(node) {
    return node.x + ',' + node.y
}

function maj(a, b) {
    if (_.get(length, b.h, Number.POSITIVE_INFINITY) > length[a.h] + map[b.y][b.x]) {
        length[b.h] = length[a.h] + map[b.y][b.x]
        prec[b.h] = a
    }
}

function sommetMin(q) {
    let min = Number.POSITIVE_INFINITY
    let sommet = undefined
    q.forEach(s => {
        if (_.get(length, s.h, Number.POSITIVE_INFINITY) < min) {
            min = length[s.h]
            sommet = s
        }
    })
    return sommet
}

let q = sommets.map(s => s)
while (q.length > 0) {
    const s = sommetMin(q)
    q = q.filter(it => it.h != s.h)
    neighbors(s.x, s.y).forEach(neighbor => {
        maj(s, neighbor)
    })
}

let end = {
    y: map.length - 1,
    x: map[0].length - 1
}
console.log(length[hash(end)])