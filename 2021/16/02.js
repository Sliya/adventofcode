const fs = require('fs')
const lazy = require('lazy.js')

const LENGTH_TYPE_BITS = '0'
const TYPES = ['SUM', 'PRODUCT', 'MINIMUM', 'MAXIMUM', 'DATA', 'GREATER', 'LESS', 'EQUAL']

const toBin = s => parseInt(s, 16).toString(2).padStart(4, '0')

const computeValue = p => {
    switch (p.type) {
        case 'SUM':
            return p.subPackets.map(sp => computeValue(sp)).reduce((a, b) => a + b, 0)
        case 'PRODUCT':
            return p.subPackets.map(sp => computeValue(sp)).reduce((a, b) => a * b, 1)
        case 'MINIMUM':
            return Math.min(...p.subPackets.map(sp => computeValue(sp)))
        case 'MAXIMUM':
            return Math.max(...p.subPackets.map(sp => computeValue(sp)))
        case 'DATA':
            return p.value
        case 'GREATER':
            return computeValue(p.subPackets[0]) > computeValue(p.subPackets[1]) ? 1 : 0
        case 'LESS':
            return computeValue(p.subPackets[0]) < computeValue(p.subPackets[1]) ? 1 : 0
        case 'EQUAL':
            return computeValue(p.subPackets[0]) == computeValue(p.subPackets[1]) ? 1 : 0
    }
}


const extractData = s => {
    let dataStr = ''
    let length = 0
    while (s.take(1).first() == 1) {
        s = s.skip(1)
        dataStr += s.take(4).join('')
        s = s.skip(4)
        length += 5
    }
    s = s.skip(1)
    dataStr += s.take(4).join('')
    s = s.skip(4)
    length += 5
    return {
        value: parseInt(dataStr, 2),
        sequence: s,
        length
    }
}

const extractSubPackets = (s, type, length) => {
    const subPackets = []
    if (type == LENGTH_TYPE_BITS) {
        s = s.take(length)
        while (!s.isEmpty() && !s.every(c => c == '0')) {
            const subPacket = extractPacket(s)
            subPackets.push(subPacket)
            s = subPacket.sequence
        }
    } else {
        for (let i = 0; i < length; i++) {
            const subPacket = extractPacket(s)
            subPackets.push(subPacket)
            s = subPacket.sequence
        }
    }
    return subPackets
}

const extractPacket = s => {
    const version = parseInt(s.take(3).join(''), 2)
    s = s.skip(3)
    const type = TYPES[parseInt(s.take(3).join(''), 2)]
    s = s.skip(3)
    if (type == 'DATA') {
        const data = extractData(s)
        return {
            version,
            type,
            value: data.value,
            sequence: data.sequence,
            length: 6 + data.length
        }
    } else {
        const lengthType = s.take(1).first()
        s = s.skip(1)
        const l = lengthType == LENGTH_TYPE_BITS ? 15 : 11
        let length = parseInt(s.take(l).join(''), 2)
        s = s.skip(l);
        const subPackets = extractSubPackets(s, lengthType, length)
        if (lengthType != LENGTH_TYPE_BITS) {
            length = subPackets.map(sp => sp.length).reduce((a, b) => a + b, 0)
        }
        s = s.skip(length)
        return {
            version,
            type,
            lengthType,
            length: length + 7 + l,
            subPackets,
            sequence: s
        }
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = lazy(data.trim()).split('').map(toBin).map(s => lazy(s)).flatten()
    const packet = extractPacket(input)
    console.log(computeValue(packet))
})