const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const gamma = parseInt(data.trim().split('\n')
        .map(it => it.split('')
            .map(c => c == '0' ? -1 : 1))
        .reduce((acc, e) => acc.map((sum, idx) => sum + e[idx]), [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        .map(e => e > 0 ? 1 : 0).join(''), 2)
    const epsilon = ~gamma << 20 >>> 20
    console.log(gamma * epsilon)
})
