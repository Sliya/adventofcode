const fs = require('fs')

function filter(list, bitPosition, criteria) {
    if (list.length == 1) {
        return list[0].join('')
    }
    let zeros = []
    let ones = []
    list.forEach(e => {
        if (e[bitPosition] == 0)
            zeros.push(e)
        else
            ones.push(e)
    })
    if (criteria == 1)
        return filter(zeros.length > ones.length ? zeros : ones, bitPosition + 1, criteria)
    else
        return filter(zeros.length > ones.length ? ones : zeros, bitPosition + 1, criteria)
}

fs.readFile('input', 'utf8', (error, data) => {
    const values = data.trim().split('\n')
        .map(it => it.split(''))
    const oxygen = parseInt(filter(values, 0, 1), 2)
    const co2 = parseInt(filter(values, 0, 0), 2)
    console.log(co2 * oxygen)
})
