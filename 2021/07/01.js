const fs = require('fs')

function computeMin(crabs, currentMin, position) {
    let score = 0;
    for (let i = 0; i < crabs.length && score < currentMin; i++) {
        score += Math.abs(position - crabs[i])
    }
    return Math.min(currentMin, score)
}

fs.readFile('input', 'utf8', (error, data) => {
    const crabs = data.trim().split(',').map(it => +it)
    const max = Math.max(...crabs)
    console.log(Array(max).fill().map((_, idx) => idx).reduce((acc, e) => computeMin(crabs, acc, e), Infinity))

})