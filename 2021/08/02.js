const fs = require('fs')

function sameCables(from, to) {
    return from.split('').every(d => to.includes(d))
}

function decoder(digits) {
    const map = {}
    const pam = Array(10)
    digits.forEach(d => {
        if (d.length == 2) {
            map[d] = 1
            pam[1] = d
        } else if (d.length == 3) {
            map[d] = 7
            pam[7] = d
        } else if (d.length == 4) {
            map[d] = 4
            pam[4] = d
        } else if (d.length == 7) {
            map[d] = 8
            pam[8] = d
        }
    })
    digits.forEach(d => {
        if (d.length == 6) {
            if (sameCables(pam[4], d)) {
                map[d] = 9
                pam[9] = d
            } else if (sameCables(pam[7], d)) {
                map[d] = 0
                pam[0] = d
            } else {
                map[d] = 6
                pam[6] = d
            }
        } else if (d.length == 5) {
            if (sameCables(pam[7], d)) {
                map[d] = 3
                pam[3] = d
            } else {
                map[d] = 2
                pam[2] = d
            }
        }
    })
    digits.forEach(d => {
        if (d.length == 5 && sameCables(d, pam[6])) {
            map[d] = 5
        }
    })
    return map
}

function decode(map, d) {
    for (k in map) {
        if (sameCables(d, k) && sameCables(k, d)) {
            return map[k]
        }

    }
}

fs.readFile('input', 'utf8', (error, data) => {
    console.log(data.trim()
        .split('\n')
        .map(line => line.split(' | ')
            .map(it => it.split(' ')))
        .map(it => {
            return {
                map: decoder(it[0]),
                code: it[1]
            }
        })
        .map(it => 1 * (it.code.map(c => decode(it.map, c)).join('')))
        .reduce((acc, e) => acc + e))
})
