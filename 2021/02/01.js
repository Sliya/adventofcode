const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n').map(it => it.split(' '))
    let x = 0
    let y = 0
    input.forEach(it => {
        if (it[0] == "forward") {
            x += +it[1]
        } else if (it[0] == "down") {
            y += +it[1]
        } else if (it[0] == "up") {
            y -= +it[1]
        }
    })
    console.log(x * y)
})