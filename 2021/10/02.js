const fs = require('fs')

const score = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}

const inverse = {
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<',
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}

function parse(line) {
    const toClose = []
    for (const c of line.split('')) {
        switch (c) {
            case '(':
            case '[':
            case '{':
            case '<':
                toClose.push(c)
                break;
            default:
                const expected = inverse[toClose.pop()]
                if (expected != c) {
                    return 0
                }
        }
    }
    return toClose.reverse().map(c => inverse[c]).map(c => score[c]).reduce((acc, e) => 5 * acc + e)
}

fs.readFile('input', 'utf8', (error, data) => {
    const scores = data.trim().split('\n').map(l => parse(l)).filter(it => it != 0).sort((a, b) => b - a)
    console.log(scores[Math.floor(scores.length / 2)])
})