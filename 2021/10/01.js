const fs = require('fs')

const score = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

const inverse = {
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<',
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}

function parse(line) {
    const toClose = []
    for (const c of line.split('')) {
        switch (c) {
            case '(':
            case '[':
            case '{':
            case '<':
                toClose.push(c)
                break;
            default:
                const expected = inverse[toClose.pop()]
                if (expected != c) {
                    return score[c]
                }
        }
    }
    return 0
}

fs.readFile('input', 'utf8', (error, data) => {
    console.log(data.trim().split('\n').map(l => parse(l)).reduce((a, b) => a + b))
})
