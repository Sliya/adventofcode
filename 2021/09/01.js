const fs = require('fs')

function inside(grid, i, j) {
    return i >= 0 && i < grid.length && j >= 0 && j < grid[0].length
}

function isLowPoint(grid, x, y) {
    if (grid[y][x] == 9) return false
    for (i = y - 1; i <= y + 1; i++) {
        for (j = x - 1; j <= x + 1; j++) {
            if ((i == y || j == x) && inside(grid, i, j) && grid[i][j] < grid[y][x]) {
                return false
            }
        }
    }
    return true
}

fs.readFile('input2', 'utf8', (error, data) => {
    const grid = data.trim().split('\n').map(it => it.split('').map(that => +that))
    let risk = 0;
    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[0].length; j++) {
            if (isLowPoint(grid, j, i)) {
                risk += grid[i][j] + 1
            }
        }
    }
    console.log(risk)
})