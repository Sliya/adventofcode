const fs = require('fs')

function inside(grid, i, j) {
    return i >= 0 && i < grid.length && j >= 0 && j < grid[0].length
}

function isLowPoint(grid, x, y) {
    if (grid[y][x] == 9) return false
    for (i = y - 1; i <= y + 1; i++) {
        for (j = x - 1; j <= x + 1; j++) {
            if ((i == y || j == x) && inside(grid, i, j) && grid[i][j] < grid[y][x]) {
                return false
            }
        }
    }
    return true
}

function basinSize(grid, x, y) {
    const checked = new Set()
    const toCheck = [{
        x,
        y
    }]
    let res = 0
    checked.add(x + ';' + y)
    while (toCheck.length > 0) {
        let point = toCheck.pop()
        res++
        for (i = point.y - 1; i <= point.y + 1; i++) {
            for (j = point.x - 1; j <= point.x + 1; j++) {
                if ((i == point.y || j == point.x) && !checked.has(j + ';' + i) && inside(grid, i, j) && grid[i][j] != 9) {
                    checked.add(j + ';' + i)
                    toCheck.push({
                        x: j,
                        y: i
                    })
                }
            }
        }

    }
    return res
}

fs.readFile('input', 'utf8', (error, data) => {
    const grid = data.trim().split('\n').map(it => it.split('').map(that => +that))
    const basinSizes = []
    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[0].length; j++) {
            if (isLowPoint(grid, j, i)) {
                basinSizes.push(basinSize(grid, j, i))
            }
        }
    }

    console.log(basinSizes.sort((a, b) => b - a).slice(0, 3).reduce((a, b) => a * b, 1))
})