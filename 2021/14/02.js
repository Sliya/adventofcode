const fs = require('fs')
const _ = require('lodash')

function byPair(array) {
    const ret = []
    for (let i = 0; i < array.length - 1; i++) {
        ret.push(array[i] + array[i + 1])
    }
    return ret
}

function count(count, rules) {
    const ret = _.pickBy(count, (v, k) => k.length == 1)
    Object.keys(count).forEach(k => {
        if (k.length == 1) return
        const newChar = rules[k]
        const p1 = k.charAt(0) + newChar
        const p2 = newChar + k.charAt(1)
        ret[p1] = _.get(ret, p1, 0) + count[k]
        ret[p2] = _.get(ret, p2, 0) + count[k]
        ret[newChar] = _.get(ret, newChar, 0) + count[k]
    })
    return ret
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    let chain = _.merge(_.countBy(input[0].split('')), _.countBy(byPair(input[0].split(''))))
    const rules = input.slice(2).map(it => it.split(' -> ')).reduce((acc, e) => {
        acc[e[0]] = e[1];
        return acc
    }, {})
    for (let i = 0; i < 40; i++) {
        chain = count(chain, rules)
    }
    const group = Object.keys(chain).filter(it => it.length == 1).map(it => chain[it])
    const max = Math.max(...group)
    const min = Math.min(...group)
    console.log(max - min)
})