const fs = require('fs')
const _ = require('lodash')

function byPair(array) {
    const ret = []
    for (let i = 0; i < array.length - 1; i++) {
        ret.push([array[i], array[i + 1]])
    }
    return ret
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    let chain = input[0].split('')
    const rules = input.slice(2).map(it => it.split(' -> ')).reduce((acc, e) => {
        acc[e[0]] = e[1];
        return acc
    }, {})
    for (let i = 0; i < 10; i++) {
        chain = byPair(chain)
            .map(it => [it[0], rules[it[0] + it[1]]])
            .flat()
            .concat([chain[chain.length - 1]])
    }
    const group = _.countBy(chain)
    const max = Math.max(...Object.values(group))
    const min = Math.min(...Object.values(group))
    console.log(max - min)
})