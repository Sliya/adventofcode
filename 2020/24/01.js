const fs = require('fs')

function toCoords(path) {
    const c = [0, 0]
    path.forEach(step => {
        if (step == 'e') {
            c[0]++
        } else if (step == 'w') {
            c[0]--
        } else if (step == 'ne') {
            c[1]++
        } else if (step == 'nw') {
            c[0]--
            c[1]++
        } else if (step == 'sw') {
            c[1]--
        } else {
            c[0]++
            c[1]--
        }
    })
    return c.join(',')
}

function parse(str) {
    let i = 0
    let ret = []
    while (i < str.length) {
        if (str[i] == 'n' || str[i] == 's') {
            ret.push(str.substring(i, i + 2))
            i += 2
        } else {
            ret.push(str[i])
            i++
        }
    }
    return ret
}

fs.readFile('input', 'utf8', (error, data) => {
    const tiles = {}
    data.trim()
        .split('\n')
        .map(parse)
        .map(toCoords)
        .forEach(c => tiles[c] = tiles[c] ? tiles[c] + 1 : 1)
    console.log(Object.entries(tiles).filter(e => e[1] % 2 == 1).length)
})