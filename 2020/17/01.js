const fs = require('fs')

function getValue(grid, x, y, z) {
    if (z >= 0 && z < grid.length && x >= 0 && x < grid[z].length && y >= 0 && y < grid[z][x].length) {
        return grid[z][x][y]
    }
    return '.'
}

function neighbors(grid, x, y, z) {
    const ret = []
    for (let zz = z - 1; zz <= z + 1; zz++) {
        for (let xx = x - 1; xx <= x + 1; xx++) {
            for (let yy = y - 1; yy <= y + 1; yy++) {
                if (zz == z && yy == y && xx == x) {
                    continue
                }
                ret.push(getValue(grid, xx, yy, zz))
            }
        }
    }
    return ret
}

function cycle(grid) {
    const newGrid = []
    for (let z = 0; z < grid.length + 2; z++) {
        newGrid.push([])
        for (let x = 0; x < grid[0].length + 2; x++) {
            newGrid[z].push([])
            for (let y = 0; y < grid[0][1].length + 2; y++) {
                newGrid[z][x].push('.')
            }
        }
    }
    for (let z = 0; z < newGrid.length; z++) {
        for (let x = 0; x < newGrid[z].length; x++) {
            for (let y = 0; y < newGrid[z][x].length; y++) {
                const nbActiveNeighbors = neighbors(grid, x - 1, y - 1, z - 1).filter(it => it == '#').length
                if (getValue(grid, x - 1, y - 1, z - 1) == '#') {
                    if (nbActiveNeighbors == 2 || nbActiveNeighbors == 3) {
                        newGrid[z][x][y] = '#'
                    } else {
                        newGrid[z][x][y] = '.'
                    }
                } else if (nbActiveNeighbors == 3) {
                    newGrid[z][x][y] = '#'
                } else {
                    newGrid[z][x][y] = '.'
                }
            }
        }
    }
    return newGrid
}

fs.readFile('input', 'utf8', (error, data) => {
    let grid = [data.trim().split('\n').map(it => it.split(''))]
    for (let i = 0; i < 6; i++) {
        grid = cycle(grid)
    }
    console.log(grid.flat(2).filter(it => it == '#').length)
})