const fs = require('fs')

function getValue(grid, x, y, z, w) {
    if (w >= 0 && w < grid.length && z >= 0 && z < grid[w].length && x >= 0 && x < grid[w][z].length && y >= 0 && y < grid[w][z][x].length) {
        return grid[w][z][x][y]
    }
    return '.'
}

function neighbors(grid, x, y, z, w) {
    const ret = []
    for (let ww = w - 1; ww <= w + 1; ww++) {
        for (let zz = z - 1; zz <= z + 1; zz++) {
            for (let xx = x - 1; xx <= x + 1; xx++) {
                for (let yy = y - 1; yy <= y + 1; yy++) {
                    if (ww == w && zz == z && yy == y && xx == x) {
                        continue
                    }
                    ret.push(getValue(grid, xx, yy, zz, ww))
                }
            }
        }
    }
    return ret
}

function cycle(grid) {
    const newGrid = []
    for (let w = 0; w < grid.length + 2; w++) {
        newGrid.push([])
        for (let z = 0; z < grid[0].length + 2; z++) {
            newGrid[w].push([])
            for (let x = 0; x < grid[0][0].length + 2; x++) {
                newGrid[w][z].push([])
                for (let y = 0; y < grid[0][0][1].length + 2; y++) {
                    newGrid[w][z][x].push('.')
                }
            }
        }
    }
    for (let w = 0; w < newGrid.length; w++) {
        for (let z = 0; z < newGrid[w].length; z++) {
            for (let x = 0; x < newGrid[w][z].length; x++) {
                for (let y = 0; y < newGrid[w][z][x].length; y++) {
                    const nbActiveNeighbors = neighbors(grid, x - 1, y - 1, z - 1, w - 1).filter(it => it == '#').length
                    if (getValue(grid, x - 1, y - 1, z - 1, w - 1) == '#') {
                        if (nbActiveNeighbors == 2 || nbActiveNeighbors == 3) {
                            newGrid[w][z][x][y] = '#'
                        } else {
                            newGrid[w][z][x][y] = '.'
                        }
                    } else if (nbActiveNeighbors == 3) {
                        newGrid[w][z][x][y] = '#'
                    } else {
                        newGrid[w][z][x][y] = '.'
                    }
                }
            }
        }
    }
    return newGrid
}

fs.readFile('input', 'utf8', (error, data) => {
    let grid = [
        [data.trim().split('\n').map(it => it.split(''))]
    ]
    for (let i = 0; i < 6; i++) {
        grid = cycle(grid)
    }
    console.log(grid.flat(3).filter(it => it == '#').length)
})