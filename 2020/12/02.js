const fs = require('fs')

const ship = {
    NS: 0,
    EW: 0,
    F: 'E'
}

const waypoint = {
    NS: 1,
    EW: 10
}

const dir = {
    N: n => waypoint.NS += n,
    S: n => waypoint.NS -= n,
    E: n => waypoint.EW += n,
    W: n => waypoint.EW -= n,
    F: n => {
        ship.NS += n * waypoint.NS
        ship.EW += n * waypoint.EW
    },
    L: n => dir.R(360 - n),
    R: n => {
        while (n > 0) {
            [waypoint.NS, waypoint.EW] = [-waypoint.EW, waypoint.NS]
            n -= 90
        }
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data
        .trim()
        .split('\n')
        .forEach(it => {
            dir[it[0]](+it.substring(1))
        })
    console.log(Math.abs(ship.NS) + Math.abs(ship.EW))
})
