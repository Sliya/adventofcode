const fs = require('fs')

const ship = {
    NS: 0,
    EW: 0,
    F: 'E'
}

const nextDir = {
    N: 'E',
    E: 'S',
    S: 'W',
    W: 'N'
}

const dir = {
    N: n => ship.NS += n,
    S: n => ship.NS -= n,
    E: n => ship.EW += n,
    W: n => ship.EW -= n,
    F: n => dir[ship.F](n),
    L: n => dir.R(360 - n),
    R: n => {
        while (n > 0) {
            ship.F = nextDir[ship.F]
            n -= 90
        }
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data
        .trim()
        .split('\n')
        .forEach(it => {
            dir[it[0]](+it.substring(1))
        })
    console.log(Math.abs(ship.NS) + Math.abs(ship.EW))
})