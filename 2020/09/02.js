const solve = function(arr, sum) {
    for (let i = 0; i < arr.length - 1; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            const sub = arr.slice(i, j)
            const s = sub.reduce((a, b) => a + b)
            if (s == sum) {
                return sub.reduce((a, b) => a > b ? a : b) + sub.reduce((a, b) => a < b ? a : b)
            } else if (s > sum) {
                break;
            }
        }
    }
}

const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    console.log(solve(data.trim()
        .split('\n')
        .map(it => +it),
        32321523))
})