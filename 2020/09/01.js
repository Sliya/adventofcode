Array.prototype.hasSum = function(sum) {
    const sorted = this.sort((a, b) => a - b)
    let i = 0
    let j = sorted.length - 1
    while (i < j) {
        if (sorted[i] + sorted[j] == sum) {
            return true
        } else if (sorted[i] + sorted[j] < sum) {
            i++
        } else {
            j--
        }
    }
    return false
}

const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    console.log(data.trim()
        .split('\n')
        .map(it => +it)
        .find((e, i, arr) => i >= 25 && !arr.slice(i - 25, i).hasSum(e)))
})