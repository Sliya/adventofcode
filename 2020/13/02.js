const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const buses = data.trim().split('\n')[1]
        .split(',')
		.map((busId, idx) => {
			const id = +busId
			const offset = (((id-idx)%id)+id)%id
			return {id, offset}
		})
        .filter(it => !Number.isNaN(it.id))
	// Chinese remainder theorem
	const n = buses.map(b => b.id).reduce((a,b)=>a*b)
	const nhati = buses.map(b => n/b.id)
	const ei = nhati.map((nhat, i) => {
		let k = 1
		while(k*nhat % buses[i].id != 1){
			k++
		}
		return k*nhat
	})
	console.log(buses)
	console.log(n, nhati, ei)
    console.log(buses.map((b, i) => b.offset*ei[i]).reduce((a,b) => a+b)%n)
	console.log(836024966345345)
})
