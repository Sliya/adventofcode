const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    const time = +input[0]
    const next = id => Math.ceil(time / id) * id
    const busId = input[1]
        .split(',')
        .filter(it => it != 'x')
        .map(it => +it)
        .sort((a, b) => next(a) - next(b))[0]
    console.log(busId * (next(busId) - time))
})
