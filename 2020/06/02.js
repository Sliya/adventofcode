const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    console.log(data.trim()
        .split('\n\n')
        .map(grp => grp.split('\n').map(answer => answer.split('')))
        .map(grp => grp.shift().filter(item => grp.every(answer => answer.includes(item))))
        .map(commonAnswers => commonAnswers.length)
        .reduce((a, b) => a + b))
})