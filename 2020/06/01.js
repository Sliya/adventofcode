const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    console.log(data.trim()
        .split('\n\n')
        .map(it => it.split('\n').join(''))
        .map(it => new Set(it.split('')).size)
        .reduce((a, b) => a + b, 0)
    )
})