const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n').map(it => +it)
    input.sort((a, b) => a - b)
    out:
        for (let k = 0; k < input.length; k++) {
            let i = k + 1,
                j = input.length - 1
            while (i < j) {
                if (input[k] + input[i] + input[j] == 2020) {
                    console.log(input[k] * input[i] * input[j])
                    break out
                } else if (input[k] + input[i] + input[j] < 2020) {
                    i++
                } else {
                    j--
                }
            }
        }
})
