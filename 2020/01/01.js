const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n').map(it => +it)
    input.sort((a, b) => a - b)
    let i = 0,
        j = input.length - 1
    while (i < j) {
        if (input[i] + input[j] == 2020) {
            console.log(input[i] * input[j])
            break
        } else if (input[i] + input[j] < 2020) {
            i++
        } else {
            j--
        }
    }
})
