const fs = require('fs')
const rules = {}
const tickets = []

function generatePredicate(range) {
    return function(n) {
        return (n >= range[0][0] && n <= range[0][1]) || (n >= range[1][0] && n <= range[1][1])
    }
}

function parseRule(str) {
    const [name, value] = str.split(': ')
    const rangesStr = value.split(' or ')
    const ranges = rangesStr.map(r => r.split('-').map(e => +e))
    rules[name] = generatePredicate(ranges)
}

function parseTicket(str) {
    tickets.push(str.split(',').map(e => +e))
}

fs.readFile('input', 'utf8', (error, data) => {
    let parser = parseRule
    const input = data
        .trim()
        .split('\n')
        .forEach(str => {
            if (str == '') {
                return
            }
            if (str == 'your ticket:' || str == 'nearby tickets:') {
                parser = parseTicket
                return
            }
            parser(str)
        })

    const validTickets = tickets.slice(1)
        .filter(t =>
            !t.some(v =>
                !Object.values(rules).some(p => p(v))
            )
        )
    const possibleRules = tickets[0].map(t => Object.keys(rules))
    validTickets.forEach(t => {
        t.forEach((v, i) => {
            possibleRules[i] = possibleRules[i].filter(rk => rules[rk](v))
        })
    })
    while (!possibleRules.every(r => r.length == 1)) {
        possibleRules.filter(r => r.length == 1)
            .map(a => a[0])
            .forEach(r => {
                possibleRules.forEach(pr => {
                    if (pr.length == 1) return
                    if (!pr.includes(r)) return
                    pr.splice(pr.indexOf(r), 1)
                })
            })
    }
    console.log(possibleRules
        .map((r, i) => r[0].startsWith('departure') ? tickets[0][i] : 1)
        .reduce((a, b) => a * b))
})