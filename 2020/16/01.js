const fs = require('fs')

const rules = {}
const tickets = []

function generatePredicate(range) {
    return function(n) {
        return (n >= range[0][0] && n <= range[0][1]) || (n >= range[1][0] && n <= range[1][1])
    }
}

function parseRule(str) {
    const [name, value] = str.split(': ')
    const rangesStr = value.split(' or ')
    const ranges = rangesStr.map(r => r.split('-').map(e => +e))
    rules[name] = generatePredicate(ranges)
}

function parseTicket(str) {
    tickets.push(str.split(',').map(e => +e))
}

fs.readFile('input', 'utf8', (error, data) => {
    let parser = parseRule
    const input = data
        .trim()
        .split('\n')
        .forEach(str => {
            if (str == '') {
                return
            }
            if (str == 'your ticket:' || str == 'nearby tickets:') {
                parser = parseTicket
                return
            }
            parser(str)
        })

    console.log(tickets.slice(1)
        .map(t =>
            t.filter(v =>
                !Object.values(rules).some(p => p(v))
            )
        )
        .flat()
        .reduce((a, b) => a + b))
})