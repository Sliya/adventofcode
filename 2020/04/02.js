const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const requirements = {
        'byr': byr => byr >= 1920 && byr <= 2002,
        'iyr': iyr => iyr >= 2010 && iyr <= 2020,
        'eyr': eyr => eyr >= 2020 && eyr <= 2030,
        'hgt': hgt => {
            const h = hgt.substring(0, hgt.length - 2)
            if (hgt.slice(-2) == 'cm') {
                return h >= 150 && h <= 193
            } else if (hgt.slice(-2) == 'in') {
                return h >= 59 && h <= 76
            }
            return false
        },
        'hcl': hcl => hcl.match(/^#[a-f0-9]{6}$/),
        'ecl': ecl => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(ecl),
        'pid': pid => pid.match(/^[0-9]{9}$/)
    }

    const docs = data.trim().split('\n\n')
    console.log(docs.map(it => it.split('\n'))
        .map(it => it.flat())
        .map(it => it.map(e => e.split(' ')))
        .map(it => it.flat())
        .map(it => it.map(e => e.split(':')))
        .map(it => Object.fromEntries(new Map(it)))
        .filter(it => Object.entries(requirements).every(([k, v]) => it[k] && v(it[k])))
        .length)
})