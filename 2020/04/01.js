const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const requirements = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    const docs = data.trim().split('\n\n')
    console.log(docs.map(it => it.split('\n'))
        .map(it => it.flat())
        .map(it => it.map(e => e.split(' ')))
        .map(it => it.flat())
        .map(it => it.map(e => e.split(':')[0]))
        .filter(it => requirements.every(req => it.includes(req)))
        .length
    )
})