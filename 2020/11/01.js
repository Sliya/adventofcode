const fs = require('fs')
const max = Math.max
const min = Math.min

function isValid(i, j, h, w) {
    return i >= 0 && i < h && j >= 0 && j < w
}

function seatsAround(cells, i, j) {
    const h = cells.length
    const w = cells[0].length
    const seats = []
    for (let di = -1; di <= 1; di++) {
        for (let dj = -1; dj <= 1; dj++) {
            if (!isValid(i + di, j + dj, h, w) || (di == 0 && dj == 0)) {
                continue
            }
            if (cells[i + di][j + dj] != '.') {
                seats.push(cells[i + di][j + dj])
            }
        }
    }
    return seats
}

function compute(cells, i, j) {
    if (cells[i][j] == '.') return '.'
    const nbOccupied = seatsAround(cells, i, j).filter(s => s == '#').length
    if (cells[i][j] == 'L' && nbOccupied == 0) {
        return '#'
    } else if (cells[i][j] == '#' && nbOccupied > 3) {
        return 'L'
    }
    return cells[i][j]
}

function turn(cells) {
    let hasChanged = false
    const newCells = []
    for (let i = 0; i < cells.length; i++) {
        const line = []
        for (let j = 0; j < cells[i].length; j++) {
            const newCell = compute(cells, i, j)
            if (newCell != cells[i][j]) {
                hasChanged = true
            }
            line.push(newCell)
        }
        newCells.push(line)
    }
    return {
        hasChanged,
        newCells
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data
        .trim()
        .split('\n')
        .map(it => it.split(''))
    let res = turn(input)
    while (res.hasChanged) {
        res = turn(res.newCells)
    }
    console.log(res.newCells.flat().filter(it => it == '#').length)
})
