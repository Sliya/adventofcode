function seatNumber(s) {
    const chars = {
        F: '0',
        B: '1',
        L: '0',
        R: '1'
    }
    const regex = /[FBLR]/g
    const row = parseInt(s.substring(0, 7).replace(regex, m => chars[m]), 2)
    const col = parseInt(s.substring(7).replace(regex, m => chars[m]), 2)
    return row * 8 + col
}

const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const seats = data.trim().split('\n')
        .map(seatNumber)
        .sort((a, b) => a - b)
    for (let i = 0; i < seats.length - 1; i++) {
        if (seats[i] + 1 != seats[i + 1]) {
            console.log(seats[i] + 1)
            break
        }
    }
})