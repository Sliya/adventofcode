const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    let acc = 0
    let i = 0
    const instructions = data.trim()
        .split('\n')
        .map(it => it.split(' '))
    const ops = {
        'nop': () => {
            i++
        },
        'jmp': n => {
            i += n
        },
        'acc': n => {
            acc += n
            i++
        }
    }
    let visited = new Set();
    while (!visited.has(i)) {
        visited.add(i)
        const [op, arg] = instructions[i]
        ops[op](parseInt(arg))
    }
    console.log(acc)
})