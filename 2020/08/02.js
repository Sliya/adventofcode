const fs = require('fs')

function tryToSolve(instructions, acc, i, visited) {
    const ops = {
        'nop': () => {
            i++
        },
        'jmp': n => {
            i += n
        },
        'acc': n => {
            acc += n
            i++
        }
    }
    while (!visited.has(i) && i < instructions.length) {
        visited.add(i)
        const [op, arg] = instructions[i]
        ops[op](parseInt(arg))
    }
    return {
        acc: acc,
        success: i >= instructions.length
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    let acc = 0
    let i = 0
    const instructions = data.trim()
        .split('\n')
        .map(it => it.split(' '))
    const ops = {
        'nop': () => {
            i++
        },
        'jmp': n => {
            i += n
        },
        'acc': n => {
            acc += n
            i++
        }
    }
    let visited = new Set();
    while (!visited.has(i)) {
        if (instructions[i][0] == 'jmp') {
            instructions[i][0] = 'nop'
            const ret = tryToSolve(instructions, acc, i, new Set(visited))
            if (ret.success) {
                acc = ret.acc
                break
            }
            instructions[i][0] = 'jmp'
        } else if (instructions[i][0] == 'nop') {
            instructions[i][0] = 'jmp'
            const ret = tryToSolve(instructions, acc, i, new Set(visited))
            if (ret.success) {
                acc = ret.acc
                break
            }
            instructions[i][0] = 'nop'

        }
        visited.add(i)
        const [op, arg] = instructions[i]
        ops[op](parseInt(arg))

    }
    console.log(acc)
})