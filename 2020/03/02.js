const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const nbTrees = [0, 0, 0, 0, 0]
    const ws = [0, 0, 0, 0, 0]
    const hs = [0, 0, 0, 0, 0]
    const slopes = [
        [1, 1],
        [3, 1],
        [5, 1],
        [7, 1],
        [1, 2]
    ]
    const lines = data.trim().split('\n')
    const width = lines[0].length
    const height = lines.length
    while (hs.some(it => it < height)) {
        slopes.filter((e, i) => hs[i] < height)
            .forEach((s, i) => {
                nbTrees[i] += lines[hs[i]][ws[i]] == "#" ? 1 : 0
                hs[i] += s[1]
                ws[i] = (ws[i] + s[0]) % width
            })
    }
    console.log(nbTrees.reduce((a, b) => a * b, 1))
})
