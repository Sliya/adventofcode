const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    let nbTrees = 0
    const lines = data.trim().split('\n')
    const width = lines[0].length
    const height = lines.length
    for (let w = 0, h = 0; h < height; h++, w = (w + 3) % width) {
        nbTrees += lines[h][w] == '#' ? 1 : 0
    }
    console.log(nbTrees)
})
