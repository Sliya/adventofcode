const fs = require('fs')

// Expr    -> Operand Expr'
// Expr'   -> op Operand Expr' | undefined
// Operand -> Number | ( Expr )

class Expr {
    constructor(type, op1, op2) {
        this.type = type
        this.op1 = op1
        this.op2 = op2
    }

    compute() {
        if (this.type == 'NUMBER') {
            return this.op1
        } else if (this.type == 'PLUS') {
            return this.op1.compute() + this.op2.compute()
        } else if (this.type == 'MULT') {
            return this.op1.compute() * this.op2.compute()
        }
    }
}

function parseExpr(tokens) {
    const op1 = parseOperand(tokens)
    return parseExpr2(tokens, op1)
}

function parseExpr2(tokens, op1) {
    if (tokens.length == 0) {
        return
    }
    if (['MULT', 'PLUS'].includes(tokens[0].type)) {
        const op = tokens.shift()
        const op2 = new Expr(op.type, op1, parseOperand(tokens))
        const e = parseExpr2(tokens, op2)
        return e ? e : op2
    }
}

function parseOperand(tokens) {
    if (tokens.length == 0) {
        return
    }
    const token = tokens.shift()
    if (token.type == 'NUMBER') {
        return new Expr(token.type, token.value)
    } else if (token.type == 'OPEN_PAR') {
        const expr = parseExpr(tokens)
        read(tokens, 'CLOSE_PAR')
        return expr
    }
}

function read(tokens, type) {
    const t = tokens.shift()
    if (t == undefined || t.type != type) {
        throw `Expected ${type} but got ${t ? t.type : undefined}${t ? ' at pos '+t.pos : ''}`
    }
}

function parse(str) {
    const tokens = tokenize(str)
    return parseExpr(tokens).compute()
}

function tokenize(str) {
    const tokens = []
    let curr = ''
    for (let i = 0; i < str.length; i++) {
        if (str[i] == ' ') {
            if (curr != '') {
                tokens.push({
                    type: 'NUMBER',
                    value: +curr,
                    pos: i - curr.length
                })
                curr = ''
            }
            continue
        } else if (str[i] == '(') {
            tokens.push({
                type: 'OPEN_PAR',
                value: '(',
                pos: i
            })
        } else if (str[i] == ')') {
            if (curr != '') {
                tokens.push({
                    type: 'NUMBER',
                    value: +curr,
                    pos: i
                })
            }
            curr = ''
            tokens.push({
                type: 'CLOSE_PAR',
                value: ')',
                pos: i
            })
        } else if (str[i] == '+') {
            tokens.push({
                type: 'PLUS',
                value: '+',
                pos: i
            })
        } else if (str[i] == '*') {
            tokens.push({
                type: 'MULT',
                value: '*',
                pos: i
            })
        } else {
            curr += str[i]
        }
    }
    if (curr != '') {
        tokens.push({
            type: 'NUMBER',
            value: +curr,
            pos: str.length - curr.length
        })
        curr = ''
    }
    return tokens
}

fs.readFile('input', 'utf8', (error, data) => {
    console.log(data.trim()
        .split('\n')
        .map(parse)
        .reduce((a, b) => a + b))
})