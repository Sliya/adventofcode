const fs = require('fs')
const _ = require('lodash')

function parse(str) {
    const regex = /(.*) \(contains (.*)\)/
    const arr = str.match(regex)
    return {
        ingredients: arr[1].split(' '),
        allergens: arr[2].split(', ')
    }
}

fs.readFile('input', 'utf8', (error, data) => {
    const possibilities = {}
    const dishes = data.trim()
        .split('\n')
        .map(parse)

    dishes.forEach(dish => {
        dish.allergens.forEach(allergen => {
            possibilities[allergen] = possibilities[allergen] ? _.intersection(possibilities[allergen], dish.ingredients) : dish.ingredients
        })
    })

    while (Object.values(possibilities).some(v => v.length > 1)) {
        Object.values(possibilities)
            .filter(it => it.length == 1)
            .forEach(it => {
                Object.keys(possibilities)
                    .filter(k => possibilities[k].length != 1)
                    .forEach(k => {
                        possibilities[k] = possibilities[k].filter(v => v != it)
                    })
            })
    }
    console.log(Object.keys(possibilities).sort().map(k => possibilities[k]).flat().join(','))
})