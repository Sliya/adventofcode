const fs = require('fs')
const _ = require('lodash')

function parseRule(str) {
    return str.split(': ')[1]
}

const theRules = []

function computeRuleImpl(rule) {
    if (rule == '42 | 42 8') {
        return '(?:' + computeRule('42') + ')+'
    }
    if (rule == '42 31 | 42 11 31') {
        const r42 = computeRule('42')
        const r31 = computeRule('31')
        let r = '(?:' + r42 + ')(?:' + r31 + ')'
        for (let i = 1; i < 15; i++) {
            r = r + '|(?:' + r42 + '){' + i + '}(?:' + r31 + '){' + i + '}'
        }
        return '(' + r + ')'
    }
    if (rule == '"a"') {
        return 'a'
    }
    if (rule == '"b"') {
        return 'b'
    }
    if (rule.includes('|')) {
        const rules = rule.split(' | ')
        return '(?:' + computeRule(rules[0]) + ')|(?:' + computeRule(rules[1]) + ')'
    }
    if (rule.includes(' ')) {
        return rule.split(' ').map(it => '(?:' + computeRule(it) + ')').join('')
    }
    return computeRule(theRules[rule])
}

const computeRule = _.memoize(computeRuleImpl)

function computeRules(rules) {
    return new RegExp('^' + computeRule(theRules[0]) + '$')
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    while (input[0] != '') {
        const splits = input.shift().split(': ')
        theRules[splits[0]] = splits[1]
    }
    theRules['8'] = '42 | 42 8'
    theRules['11'] = '42 31 | 42 11 31'
    input.shift()
    const theRule = computeRules()
    console.log(input.filter(i => theRule.test(i)).length)
    // 256 < x < 391
    // != 376 != 386
})