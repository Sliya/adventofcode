const fs = require('fs')
const _ = require('lodash')

function parseRule(str) {
    return str.split(': ')[1]
}

const theRules = []

function computeRuleImpl(rule) {
    if (rule.source) {
        return rule
    }
    if (rule == '"a"') {
        return new RegExp(/a/)
    }
    if (rule == '"b"') {
        return new RegExp(/b/)
    }
    if (rule.includes('|')) {
        const rules = rule.split(' | ')
        return new RegExp('(' + computeRule(rules[0]).source + ')|(' + computeRule(rules[1]).source + ')')
    }
    if (rule.includes(' ')) {
        return new RegExp(rule.split(' ').map(it => '(' + computeRule(it).source + ')').join(''))
    }
    return computeRule(theRules[rule])
}

const computeRule = _.memoize(computeRuleImpl)

function computeRules(rules) {
    return new RegExp('^' + computeRule(theRules[0]).source + '$')
}

fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    while (input[0] != '') {
        const splits = input.shift().split(': ')
        theRules[splits[0]] = splits[1]
    }
    input.shift()
    const theRule = computeRules()
    console.log(input.filter(i => theRule.test(i)).length)
})