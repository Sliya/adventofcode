const fs = require('fs')

function combat(d1, d2) {
    const history = {
        d1: [],
        d2: []
    }
    while (d1.length > 0 && d2.length > 0) {
        if (winByHistory(d1, d2, history)) {
            return 1
        }
        history.d1.push(d1.join(' '))
        history.d2.push(d2.join(' '))
        playRound(d1, d2)
    }
    return d1.length == 0 ? 2 : 1
}

function winByHistory(d1, d2, history) {
    return history.d1.includes(d1.join(' ')) || history.d2.includes(d2.join(' '))
}

function playRound(d1, d2) {
    const c1 = d1.shift()
    const c2 = d2.shift()
    let winner = c1 > c2 ? 1 : 2
    if (d1.length >= c1 && d2.length >= c2) {
        winner = combat(d1.slice(0, c1), d2.slice(0, c2))
    }
    if (winner == 1) {
        d1.push(c1)
        d1.push(c2)
    } else {
        d2.push(c2)
        d2.push(c1)
    }
}

function score(d) {
    return d.reverse().map((e, i) => (i + 1) * e).reduce((a, b) => a + b)
}

fs.readFile('input', 'utf8', (error, data) => {
    const d1 = []
    const d2 = []
    let isP1 = true
    const input = data.trim()
        .split('\n')
        .filter(it => !it.startsWith('Player'))
        .forEach(it => {
            if (it == '') {
                isP1 = false
                return
            }
            if (isP1) {
                d1.push(+it)
            } else {
                d2.push(+it)
            }
        })
    combat(d1, d2)
    console.log(d1.length == 0 ? score(d2) : score(d1))
})