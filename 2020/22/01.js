const fs = require('fs')

function playRound(d1, d2) {
    c1 = d1.shift()
    c2 = d2.shift()
    if (c1 > c2) {
        d1.push(c1)
        d1.push(c2)
    } else {
        d2.push(c2)
        d2.push(c1)
    }
}

function score(d) {
    return d.reverse().map((e, i) => (i + 1) * e).reduce((a, b) => a + b)
}

fs.readFile('input', 'utf8', (error, data) => {
    const d1 = []
    const d2 = []
    let isP1 = true
    const input = data.trim()
        .split('\n')
        .filter(it => !it.startsWith('Player'))
        .forEach(it => {
            if (it == '') {
                isP1 = false
                return
            }
            if (isP1) {
                d1.push(+it)
            } else {
                d2.push(+it)
            }
        })
    while (d1.length != 0 && d2.length != 0) {
        playRound(d1, d2)
    }
    console.log(d1.length == 0 ? score(d2) : score(d1))
})