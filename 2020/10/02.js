const fs = require('fs')
const _ = require('lodash')

const jumps = {}

function count(a){
	if(jumps[a].length == 0){
		return 1
	} else{
		let nb = 0
		jumps[a].forEach(j=> nb+=memCount(j))
		return nb
	}
}

const memCount = _.memoize(count)

fs.readFile('input', 'utf8', (error, data) => {
    const possibles = [0].concat(data.trim()
        .split('\n')
        .sort((a, b) => a - b))
	possibles.forEach((e,idx)=>{
		const j = [];
		for(let i = 1; i < 4; i++){
			if(possibles[idx+i]-e < 4){
				j.push(possibles[idx+i])
			}
		}
		jumps[e]=j
	})
	console.log(count(0))
})
