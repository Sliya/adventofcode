const fs = require('fs')
const _ = require('lodash')
fs.readFile('input', 'utf8', (error, data) => {
    const groups = _.groupBy(['0'].concat(data
            .trim()
            .split('\n'))
        .sort((a, b) => a - b)
        .map((e, i, arr) => i == arr.length - 1 ? 3 : arr[i + 1] - e))
    console.log((groups['1'].length) * groups['3'].length)
})
