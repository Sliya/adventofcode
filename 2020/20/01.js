const fs = require('fs')
const _ = require('lodash')

String.prototype.reverse = function() {
    return this.split('').reverse().join('')
}

function borders(tile) {
    const ret = []
    const columns = _.zip(...tile)
    ret.push(tile[0].join(''))
    ret.push(columns[columns.length - 1].join(''))
    ret.push(tile[tile.length - 1].join(''))
    ret.push(columns[0].join(''))
    return ret
}

function match(a, b) {
    return a.borders.some(border => b.borders.includes(border)) ||
        a.borders.map(it => it.reverse()).some(border => b.borders.includes(border))
}

function isCorner(tile, tiles) {
    return tiles
        .filter(it => it.id != tile.id)
        .filter(it => match(it, tile))
        .length == 2
}

fs.readFile('input', 'utf8', (error, data) => {
    const tiles = []
    let currentTile = []
    let currentId
    const input = data
        .trim()
        .split('\n')
        .forEach(line => {
            if (line == '') {
                tiles.push({
                    id: currentId,
                    borders: borders(currentTile)
                })
                currentTile = []
                return
            }
            if (line.startsWith('Tile ')) {
                currentId = +line.split('Tile ')[1].replace(':', '')
                return
            }
            currentTile.push(line.split(''))
        })
    tiles.push({
        id: currentId,
        borders: borders(currentTile)
    })

    console.log(tiles.filter(it => isCorner(it, tiles)).reduce((a, b) => a * b.id, 1))
})