const bags = {}
class Bag {
    constructor(color) {
        this.color = color
        this.parents = []
        this.children = []
        this.childrenNb = {}
    }

    addParent(b, nb) {
        b.children.push(this)
        b.childrenNb[this.color] = nb
        this.parents.push(b)
    }

    contains(color) {
        return this.children.some(b => b.color == color) || this.children.some(b => b.contains(color))
    }

    nbBags() {
        return Object.values(this.childrenNb).reduce((a, b) => a + b, 0) +
            this.children
            .map(it => this.childrenNb[it.color] * it.nbBags())
            .reduce((a, b) => a + b, 0)
    }

    static fromString(s) {
        const color = s.substring(0, s.indexOf(' bags'))
        if (bags[color] == undefined) {
            bags[color] = new Bag(color)
        }
        const bag = bags[color]
        s = s.split('contain ')[1]
        if (s != 'no other bags.') {
            s.split(', ')
                .map(it => it.split(' ').slice(0, 3))
                .map(it => {
                    const nb = +it[0]
                    const color = it.slice(1).join(' ')
                    if (bags[color] == undefined) {
                        bags[color] = new Bag(color)
                    }
                    bags[color].addParent(bag, nb)
                    return bags[color]
                })
        }
        return bag
    }
}

const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    data.trim()
        .split('\n')
        .map(Bag.fromString)
    console.log(bags['shiny gold'].nbBags())
})