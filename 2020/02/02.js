const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    console.log(input.map(i => {
        const s = i.split(': ')
        const n = s[0].split(' ')
        const m = n[0].split('-')
        return {
            first: m[0],
            second: m[1],
            letter: n[1],
            pwd: s[1]
        }
    }).filter(it => (it.pwd[it.first - 1] == it.letter) != (it.pwd[it.second - 1] == it.letter)).length)
})