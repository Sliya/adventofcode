const fs = require('fs')
fs.readFile('input', 'utf8', (error, data) => {
    const input = data.trim().split('\n')
    console.log(input.map(i => {
        const s = i.split(': ')
        const n = s[0].split(' ')
        const m = n[0].split('-')
        return {
            min: m[0],
            max: m[1],
            letter: n[1],
            pwd: s[1].split('').filter(it => it == n[1])
        }
    }).filter(it => it.min <= it.pwd.length && it.max >= it.pwd.length).length)
})