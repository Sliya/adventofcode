let turn = 7
const spoken = new Map()
spoken.set(2, 1)
    .set(20, 2)
    .set(0, 3)
    .set(4, 4)
    .set(1, 5)
let lastSpoken = 17
do {
    const toBeSpoken = spoken.has(lastSpoken) ? turn - 1 - spoken.get(lastSpoken) : 0
    spoken.set(lastSpoken, turn - 1)
    lastSpoken = toBeSpoken
} while (++turn <= 30000000)
console.log(lastSpoken)