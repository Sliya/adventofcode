const fs = require('fs')

function computeEncryptionKey(pubKey1, pubKey2) {
    const mod = 20201227
    const computeLoopSize = function(subjectNumber, currentValue, key) {
        let loopSize = 0
        while (key !== currentValue) {
            loopSize++
            currentValue = currentValue * subjectNumber % mod
        }
        return loopSize
    }
    const ls = computeLoopSize(7, 1, pubKey1)

    const computeKey = function(subjectNumber, loopSize) {
        let currentValue = 1
        while (loopSize !== 0) {
            loopSize--
            currentValue = currentValue * subjectNumber % mod
        }
        return currentValue
    }
    return computeKey(pubKey2, ls)
}

fs.readFile('input', 'utf8', (error, data) => {
    const [pubk1, pubk2] = data.trim().split('\n').map(it => +it)
    console.log(computeEncryptionKey(pubk1, pubk2))
})