const fs = require('fs')

function applyMask(mask, value) {
    const padded = value.toString(2).padStart(36, 0)
    return parseInt(mask.split('')
        .map((e, i) => e == 'X' ? padded[i] : e)
        .join(''), 2)
}

fs.readFile('input', 'utf8', (error, data) => {
    let mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    let mem = []
    const regex = /mem\[([0-9]*)\] = ([0-9]*)/
    const input = data.trim()
        .split('\n')
        .forEach(it => {
            if (it.startsWith("mask")) {
                mask = it.split('mask = ')[1]
            } else {
                const [line, addr, value] = it.match(regex)
                mem[+addr] = applyMask(mask, +value)
            }
        })
    console.log(mem.reduce((a, b) => a + b))

})