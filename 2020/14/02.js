const fs = require('fs')
const _ = require('lodash')

function getAddresses(addr, mask) {
    let ret = []
    let rad = ''
    for (let i = 0; i < mask.length; i++) {
        if (mask[i] == '0') {
            rad += addr[i]
        } else if (mask[i] == '1') {
            rad += '1'
        } else {
            if (ret.length == 0) {
                ret.push(rad + '0')
                ret.push(rad + '1')
            } else {
                ret = ret.map(e => [e + rad + '0', e + rad + '1']).flat()
            }
            rad = ''
        }
    }
    if (rad != '') {
        if (ret.length == 0) {
            ret.push(rad)
        } else {
            ret = ret.map(e => e + rad)
        }
    }
    return ret.map(a => parseInt(a, 2))
}

function to36bitsStr(n) {
    return n.toString(2).padStart(36, 0)
}

fs.readFile('input', 'utf8', (error, data) => {
    let mask = '000000000000000000000000000000X1001X'
    let mem = {}
    let res = 0
    const regex = /mem\[([0-9]*)\] = ([0-9]*)/
    const input = data.trim()
        .split('\n')
        .forEach(it => {
            if (it.startsWith("mask")) {
                mask = it.split('mask = ')[1]
            } else {
                const [line, addr, value] = it.match(regex)
                getAddresses(to36bitsStr(+addr), mask)
                    .forEach(m => {
                        if (mem[m]) {
                            res -= mem[m]
                        }
                        mem[m] = +value
                        res += +value
                    })
            }
        })
    console.log(res)
})