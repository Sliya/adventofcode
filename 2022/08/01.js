const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const trees = input
    .trim()
    .split('\n')
    .map(it => it.split(''))

function isVisible(i, j) {
    let visible = true
    for (let ii = i - 1; ii >= 0; ii--) {
        if (trees[ii][j] >= trees[i][j]) {
            visible = false
            break
        }
    }
    if (visible) return true
    visible = true
    for (let ii = i + 1; ii < trees.length; ii++) {
        if (trees[ii][j] >= trees[i][j]) {
            visible = false
            break
        }
    }
    if (visible) return true
    visible = true
    for (let jj = j - 1; jj >= 0; jj--) {
        if (trees[i][jj] >= trees[i][j]) {
            visible = false
            break
        }
    }
    if (visible) return true
    visible = true
    for (let jj = j + 1; jj < trees[0].length; jj++) {
        if (trees[i][jj] >= trees[i][j]) {
            visible = false
            break
        }
    }
    return visible

}

let nbVisible = 0

for (let i = 0; i < trees.length; i++) {
    for (let j = 0; j < trees[0].length; j++) {
        if (isVisible(i, j))
            nbVisible++
    }
}

console.log(nbVisible)