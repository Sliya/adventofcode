const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const trees = input
    .trim()
    .split('\n')
    .map(it => it.split(''))

function score(i, j) {
    let a = 1
    for (let ii = i - 2; ii >= 0; ii--) {
        a++
        if (trees[ii][j] >= trees[i][j]) {
            break
        }
    }

    let b = 1
    for (let ii = i + 2; ii < trees.length; ii++) {
        b++
        if (trees[ii][j] >= trees[i][j]) {
            break
        }
    }

    let c = 1
    for (let jj = j - 2; jj >= 0; jj--) {
        c++
        if (trees[i][jj] >= trees[i][j]) {
            break
        }
    }

    let d = 1
    for (let jj = j + 2; jj < trees[0].length; jj++) {
        d++
        if (trees[i][jj] >= trees[i][j]) {
            break
        }
    }
    return a * b * c * d

}

let scoreMax = -1

for (let i = 0; i < trees.length; i++) {
    for (let j = 0; j < trees[0].length; j++) {
        const s = score(i, j)
        if (s > scoreMax)
            scoreMax = s
    }
}

console.log(scoreMax)