const fs = require('fs')

const cals = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})
console.log(cals
    .split('\n\n')
    .map(it => it.split('\n')
        .reduce((a, b) => 1 * a + 1 * b, 0))
    .sort((a, b) => b - a)
    .slice(0, 3)
    .reduce((a, b) => 1 * a + 1 * b, 0)
)