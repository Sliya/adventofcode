const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

function Graph() {

    return {
        edges: {},
        vertices: new Set(),

        addEdge: function(s, e, w) {
            if (w === Infinity) return
            this.addArc(s, e, w)
            this.addArc(e, s, w)
        },

        addArc(s, e, w) {
            this.vertices.add(s)
            this.vertices.add(e)
            if (this.edges[s] == null) {
                this.edges[s] = {}
            }
            this.edges[s][e] = w
        },

        neighbors: function(n) {
            return Object.keys(this.edges[n])
        },

        dist: function(a, b) {
            return a === b ? 0 : this.edges?.[a]?.[b] ?? Infinity
        },

        shortestPath: function(start, end) {
            const d = {}
            const visited = new Set()
            const pred = {}
            this.vertices.forEach(v => d[v] = Infinity)
            d[start] = 0
            let toVisit = [start]
            while (toVisit.length > 0) {
                const a = toVisit
                    .filter(v => !visited.has(v))
                    .reduce((a, b) => d[a] < d[b] ? a : b, 'outside')
                toVisit = toVisit.filter(it => it !== a)
                visited.add(a)
                this.neighbors(a)
                    .filter(v => !visited.has(v))
                    .forEach(v => {
                        if (d[v] > d[a] + this.dist(a, v)) {
                            d[v] = d[a] + this.dist(a, v)
                            pred[v] = a
                            toVisit.push(v)
                        }
                    })
            }
            const path = []
            let s = end
            while (s != start) {
                if (s == null) {
                    return []
                }
                path.push(s)
                s = pred[s]
            }
            path.push(start)
            return path.reverse()
        },

    }
}

function id(a, b) {
    return `${a},${b}`
}

function parseMap(map, weight) {

    const g = Graph()

    function neighbors(i, j) {
        const ret = []
        if (i - 1 >= 0)
            ret.push({
                id: id(i - 1, j),
                val: map[i - 1][j]
            })
        if (i + 1 < map.length)
            ret.push({
                id: id(i + 1, j),
                val: map[i + 1][j]
            })
        if (j - 1 >= 0)
            ret.push({
                id: id(i, j - 1),
                val: map[i][j - 1]
            })
        if (j + 1 < map[i].length)
            ret.push({
                id: id(i, j + 1),
                val: map[i][j + 1]
            })
        return ret
    }

    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[0].length; j++) {
            neighbors(i, j)
                .forEach(n => g.addArc(id(i, j), n.id, weight(map[i][j], n.val)))
        }
    }
    return g
}

const m = input
    .trim()
    .split('\n')
    .map(it => it.split(''))

let start = ''
let end = ''
for (let i = 0; i < m.length; i++) {
    for (let j = 0; j < m[0].length; j++) {
        if (m[i][j] === 'S') {
            m[i][j] = 'a'
            start = id(i, j)
        }
        if (m[i][j] === 'E') {
            m[i][j] = 'z'
            end = id(i, j)
        }
    }
}

function w(a, b) {
    const diff = a.charCodeAt(0) - b.charCodeAt(0)
    return diff < -1 ? Infinity : 1
}
const g = parseMap(m, w)
console.log(g.shortestPath(start, end).length - 1)