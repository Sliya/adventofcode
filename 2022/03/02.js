const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

function priority(c) {
    if (c == c.toLowerCase()) {
        return 1 + c.charCodeAt(0) - 'a'.charCodeAt(0)
    }
    return 27 + c.charCodeAt(0) - 'A'.charCodeAt(0)
}

function findCommonElement(a, b, d) {
    for (let c of a) {
        if (b.indexOf(c) != -1 && d.indexOf(c) != -1) {
            return c
        }
    }
}

console.log(
    input
    .trim()
    .split('\n')
    .reduce((acc, curr) => {
        if (acc[acc.length - 1].length == 3) {
            return [...acc, [curr]]
        }
        acc[acc.length - 1].push(curr)
        return acc
    }, [
        []
    ])
    .map(it => findCommonElement(...it))
    .map(priority)
    .reduce((a, b) => a + b, 0)
)