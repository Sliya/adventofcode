const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const pos = new Set()
pos.add("0,0")

const pH = [0, 0]
const pT = [0, 0]

function dist(a, b) {
    return Math.sqrt(Math.pow(b[0] - a[0], 2) + Math.pow(b[1] - a[1], 2))
}

function moveTailTowardsHead() {
    if (dist(pH, pT) <= Math.sqrt(2)) {
        return
    }
    pT[0] = pT[0] + (Math.sign(pH[0] - pT[0]))
    pT[1] = pT[1] + (Math.sign(pH[1] - pT[1]))

    pos.add(`${pT[0]},${pT[1]}`)
}

function move(dir, steps) {
    while (steps-- > 0) {
        switch (dir) {
            case 'R':
                pH[0]++
                break
            case 'L':
                pH[0]--
                break
            case 'U':
                pH[1]++
                break
            case 'D':
                pH[1]--
                break
        }
        moveTailTowardsHead()
    }
}

input
    .trim()
    .split('\n')
    .map(it => it.split(' '))
    .forEach(it => move(...it))

console.log(pos.size)