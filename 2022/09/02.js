const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const pos = new Set()
pos.add("0,0")
const nbOfKnots = 10

const p = Array(nbOfKnots)
    .fill(undefined)
    .map(it => [0, 0])

function dist(a, b) {
    return Math.sqrt(Math.pow(b[0] - a[0], 2) + Math.pow(b[1] - a[1], 2))
}

function moveTailTowardsHead() {
    for (let i = 0; i < nbOfKnots - 1; i++) {
        if (dist(p[i], p[i + 1]) <= Math.sqrt(2)) {
            return
        }
        p[i + 1][0] = p[i + 1][0] + (Math.sign(p[i][0] - p[i + 1][0]))
        p[i + 1][1] = p[i + 1][1] + (Math.sign(p[i][1] - p[i + 1][1]))
    }
    pos.add(`${p[nbOfKnots-1][0]},${p[nbOfKnots-1][1]}`)
}

function move(dir, steps) {
    while (steps-- > 0) {
        switch (dir) {
            case 'R':
                p[0][0]++
                break
            case 'L':
                p[0][0]--
                break
            case 'U':
                p[0][1]++
                break
            case 'D':
                p[0][1]--
                break
        }
        moveTailTowardsHead()
    }
}

input
    .trim()
    .split('\n')
    .map(it => it.split(' '))
    .forEach(it => move(...it))

console.log(pos.size)