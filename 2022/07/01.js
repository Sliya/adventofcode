const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

class File {

    constructor(s, name) {
        this.s = s
        this.name = name
    }

    size() {
        return this.s
    }


    print(tab) {
        console.log(' '.repeat(tab) + ' ' + this.name + ' ' + this.size())
    }

}

class Directory {

    constructor(p, name) {
        this.parent = p
        if (p != null) {
            p.add(this)
        }
        this.name = name
        this.entries = []
    }

    add(entry) {
        this.entries.push(entry)
    }

    size() {
        return this.entries.map(it => it.size()).reduce((a, b) => a + b, 0)
    }

    slash() {
        let d = this
        while (d.parent != null) {
            d = d.parent
        }
        return d
    }

    traverse(cb) {
        cb(this)
        this.entries
            .filter(e => e instanceof Directory)
            .forEach(e => e.traverse(cb))
    }

    print(tab) {
        console.log(' '.repeat(tab) + ' ' + this.name)
        this.entries.forEach(e => e.print(tab + 4))
    }
}

function exec(command) {
    let [prompt, cmd, args] = command.split(' ')
    if (cmd === 'cd') {
        if (args === '..') {
            currentDir = currentDir.parent
        } else {
            currentDir = new Directory(currentDir, args)
        }
    }
}

function readLs(line) {
    let [size, name] = line.split(' ')
    if (size === 'dir')
        return
    currentDir.add(new File(+size, name))
}

let currentDir = null



const lines = input
    .trim()
    .split('\n')
    .forEach(line => {
        if (line[0] === '$') {
            exec(line)
        } else {
            readLs(line)
        }
    })

let sum = 0
currentDir.slash().traverse(d => sum += d.size() < 100000 ? d.size() : 0)
console.log(sum)