const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const monkeys = []

function parseMonkey(lines) {
    lines.shift()
    const items = lines.shift().match(/\d+/g).map(it => +it)
    const opStr = lines.shift().split(': ')[1]
    const divisor = +lines.shift().match(/\d+/g)[0]
    const operation = 'newVal = (' + opStr.split('= ')[1] +
        ') % this.commonDivisor'
    const monkeyTrue = +lines.shift().match(/\d+/g)[0]
    const monkeyFalse = +lines.shift().match(/\d+/g)[0]
    lines.shift()

    return {
        items,
        divisor,
        nbInspections: 0,
        process: function() {
            this.nbInspections += this.items.length
            while (this.items.length > 0) {
                const old = this.items.shift()
                let newVal = old;
                eval(operation)
                monkeys[newVal % this.divisor == 0 ? monkeyTrue : monkeyFalse].items.push(newVal)
            }
        }
    }
}

const lines = input
    .trim()
    .split('\n')

while (lines.length >= 6) {
    monkeys.push(parseMonkey(lines))
}
const commonDivisor = monkeys.map(it => it.divisor).reduce((a, b) => a * b, 1)
monkeys.forEach(m => m.commonDivisor = commonDivisor)

for (let i = 0; i < 10000; i++) {
    monkeys.forEach(m => m.process())
}
console.log(monkeys.map(m => m.nbInspections)
    .sort((a, b) => b - a)
    .slice(0, 2)
    .reduce((a, b) => a * b, 1))