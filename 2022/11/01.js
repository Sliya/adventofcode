const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const monkeys = []

function parseMonkey(lines) {
    lines.shift()
    const items = lines.shift().match(/\d+/g).map(it => +it)
    const operation = lines.shift().split(':')[1].replace('new', 'newVal')
    const divisor = +lines.shift().match(/\d+/g)[0]
    const monkeyTrue = +lines.shift().match(/\d+/g)[0]
    const monkeyFalse = +lines.shift().match(/\d+/g)[0]
    lines.shift()

    return {
        items,
        nbInspections: 0,
        process: function() {
            this.nbInspections += this.items.length
            while (this.items.length > 0) {
                const old = this.items.shift()
                let newVal = old;
                eval(operation)
                newVal = Math.floor(newVal / 3)
                monkeys[newVal % divisor == 0 ? monkeyTrue : monkeyFalse].items.push(newVal)
            }
        }
    }
}

const lines = input
    .trim()
    .split('\n')

while (lines.length >= 6) {
    monkeys.push(parseMonkey(lines))
}

for (let i = 0; i < 20; i++) {
    monkeys.forEach(m => m.process())
}
console.log(monkeys.map(m => m.nbInspections)
    .sort((a, b) => b - a)
    .slice(0, 2)
    .reduce((a, b) => a * b, 1))