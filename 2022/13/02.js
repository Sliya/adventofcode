const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

function isOrdered(pair) {
    const [a, b] = pair
    if (typeof a === 'number' && typeof b === 'number') {
        return a === b ? 'eq' : a < b
    }
    if (typeof a === 'number' && typeof b === 'object') {
        return isOrdered([
            [a], b
        ])
    }
    if (typeof a === 'object' && typeof b === 'number') {
        return isOrdered([a, [b]])
    }
    for (let i = 0; i < a.length; i++) {
        if (i >= b.length) {
            return false
        }
        const order = isOrdered([a[i], b[i]])
        if (order !== 'eq') {
            return order
        }
    }
    return a.length === b.length ? 'eq' : true
}

console.log((input + "\n[[2]]\n[[6]]")
    .trim()
    .split('\n')
    .filter(it => it !== '')
    .map(it => JSON.parse(it))
    .sort((a, b) => isOrdered([a, b]) === false ? 1 : -1)
    .map((it, idx) => ["[[2]]", "[[6]]"].includes(JSON.stringify(it)) ? idx + 1 : 1)
    .reduce((a, b) => a * b, 1)
)