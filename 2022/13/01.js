const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

function isOrdered(pair) {
    const [a, b] = pair
    if (typeof a === 'number' && typeof b === 'number') {
        return a === b ? 'eq' : a < b
    }
    if (typeof a === 'number' && typeof b === 'object') {
        return isOrdered([
            [a], b
        ])
    }
    if (typeof a === 'object' && typeof b === 'number') {
        return isOrdered([a, [b]])
    }
    for (let i = 0; i < a.length; i++) {
        if (i >= b.length) {
            return false
        }
        const order = isOrdered([a[i], b[i]])
        if (order !== 'eq') {
            return order
        }
    }
    return a.length === b.length ? 'eq' : true
}

console.log(input
    .trim()
    .split('\n\n')
    .map(it => it.split('\n'))
    .map(it => it.map(that => JSON.parse(that)))
    .map(it => {
        return {
            val: JSON.stringify(it),
            ordered: isOrdered(it)
        }
    })
    .reduce((acc, e, idx) => e.ordered !== false ? idx + acc + 1 : acc, 0)
)