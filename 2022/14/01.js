const fs = require('fs')
const R = require('ramda')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const map = {}	
let minY = Infinity

R.pipe(
	R.trim,
	R.split('\n'),
	R.map(
		R.pipe(
			R.split(' -> '),
			R.map(R.split(','))
		)
	),
	R.aperture(2),
	R.unnest,
	R.forEach(it => {
		const [start, end] = it
		console.log(start, end)
		const [startX, startY] = start
		const [endX, endY] = end
		minY = Math.min(minY, startY, endY)
		if(startX === endX){
			map[startX] = {}
			for (let i = Math.min(startY, endY); i <= Math.max(startY,endY); i++){
				map[startX][i] = true
			}
		}else {
			for (let i = Math.min(startX, endX); i <= Math.max(startX, endX); i++){
				map[i] = {}
				map[i][startY] = true
			}
		}
	})
)

console.log(map)


