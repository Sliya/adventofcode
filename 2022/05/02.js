const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const stacks = []

function readline(line) {
    const ret = []
    let idx = 1
    while (idx < line.length) {
        ret.push(line[idx])
        idx += 4
    }
    return ret
}

function insertCrates(crates) {
    crates.forEach((e, i) => {
        if (e != ' ')
            stacks[i].unshift(e)
    })

}

function readMove(line) {
    return line.match(/\d+/g)
}

function move(quantity, from, to) {
    stacks[to - 1].push(...stacks[from - 1].splice(-1 * quantity))
}



const lines = input
    .split('\n')

let readingCrates = true

let nbOfStacks = Math.ceil(lines[0].length / 4)

while (nbOfStacks--)
    stacks.push([])

for (line of lines) {
    if (line == '' || line.startsWith(' 1')) {
        readingCrates = false
        continue
    }
    if (readingCrates)
        insertCrates(readline(line))
    else {
        move(...readMove(line))
    }
}

console.log(...stacks.map(it => it.pop()))
