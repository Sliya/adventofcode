const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

let nextPoi = 20
let sum = 0

input
    .trim()
    .split('\n')
    .map(it => it.split(' '))
    .reduce((acc, e) => {
        const nbCycle = e[0] === 'noop' ? 1 : 2
        if (acc.cycleNumber === nextPoi || acc.cycleNumber + nbCycle > nextPoi) {
            sum += acc.x * nextPoi
            nextPoi += 40
        }
        acc.cycleNumber += nbCycle
        acc.x += e[0] === 'noop' ? 0 : +e[1]
        return acc
    }, {
        cycleNumber: 1,
        x: 1
    })

console.log(sum)