const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

let x = 1
const crtWidth = 40
let crt = ''

input
    .trim()
    .split('\n')
    .map(it => {
        const s = it.split(' ')
        return {
            op: s[0],
            val: s[1]
        }
    })
    .map(it => it.op === 'addx' ? [{op: 'noop'}, it] : it)
    .flat()
    .forEach((e, cycle) => {
        console.log(cycle, x, e)
        if (cycle % crtWidth === 0) {
            crt += '\n'
        }
        if (Math.abs((cycle % crtWidth) - x) <= 1) {
            crt += '#'
        } else {
            crt += ' '
        }
        if (e.op === 'addx') {
            x += +e.val
        }
    })

console.log(crt)
