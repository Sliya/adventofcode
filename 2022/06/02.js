const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const markerSize = 14

function findMarker(msg) {
    for (let i = 0; i < msg.length - markerSize; i++) {
        if (new Set(msg.substring(i, i + markerSize).split('')).size === markerSize) {
            return i + markerSize
        }
    }
}

const lines = input
    .trim()
    .split('\n')
console.log(
    lines.map(findMarker)
)
