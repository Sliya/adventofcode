const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const score = {
    'A': {
        'X': 4,
        'Y': 8,
        'Z': 3,
    },
    'B': {
        'X': 1,
        'Y': 5,
        'Z': 9,
    },
    'C': {
        'X': 7,
        'Y': 2,
        'Z': 6,
    },
}

console.log(
    input
    .trim()
    .split('\n')
    .map(round => score[round.charAt(0)][round.charAt(2)])
    .reduce((a, b) => a + b, 0)
)
