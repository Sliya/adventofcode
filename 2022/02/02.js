const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

const score = {
    'A': {
        'X': 3,
        'Y': 4,
        'Z': 8,
    },
    'B': {
        'X': 1,
        'Y': 5,
        'Z': 9,
    },
    'C': {
        'X': 2,
        'Y': 6,
        'Z': 7,
    },
}

console.log(
    input
    .trim()
    .split('\n')
    .map(round => score[round.charAt(0)][round.charAt(2)])
    .reduce((a, b) => a + b, 0)
)