const fs = require('fs')

const input = fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
})

function overlaps(sections) {
    const [a, b] = sections
    return (a[1] >= b[0] && a[1] <= b[1]) || (a[0] >= b[0] && a[0] <= b[1]) ||
        (b[1] >= a[0] && b[1] <= a[1]) || (b[0] >= a[0] && b[0] <= a[1])
}

console.log(
    input
    .trim()
    .split('\n')
    .map(it => it.split(',')
        .map(that => that.split('-').map(t => +t)))
    .filter(it => overlaps(it))
    .length
)