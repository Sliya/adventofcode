const fs = require('fs')

const canBeSolved = (total, numbers) => {
  if (numbers.length == 1) {
    return total == numbers[0]
  }
  return canBeSolved(total, [numbers[0] + numbers[1], ...numbers.slice(2)]) || canBeSolved(total, [numbers[0] * numbers[1], ...numbers.slice(2)])
}

console.log(
  fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
  })
    .trim()
    .split('\n')
    .map(it => {
      const [total, numbers] = it.split(': ')
      return [+total, numbers.split(' ').map(that => +that)]
    })
    .filter(it => canBeSolved(...it))
    .map(it => it[0])
    .reduce((a, b) => a + b, 0)
)

