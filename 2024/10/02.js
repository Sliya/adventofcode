const fs = require('fs')

const map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split('')
    .map(that => +that))

const isStart = (i, j) => map[i][j] == 0

const numberOfTrailheads = (i, j) => {
  const toVisit = [{ i, j }]
  let headsNb = 0
  while (toVisit.length > 0) {
    const n = toVisit.pop()
    if (map[n.i][n.j] == 9) {
      headsNb++
      continue
    }
    if (n.i - 1 >= 0 && map[n.i - 1][n.j] == map[n.i][n.j] + 1) {
      toVisit.push({ i: n.i - 1, j: n.j })
    }
    if (n.i + 1 < map.length && map[n.i + 1][n.j] == map[n.i][n.j] + 1) {
      toVisit.push({ i: n.i + 1, j: n.j })
    }
    if (n.j - 1 >= 0 && map[n.i][n.j - 1] == map[n.i][n.j] + 1) {
      toVisit.push({ i: n.i, j: n.j - 1 })
    }
    if (n.j + 1 < map[0].length && map[n.i][n.j + 1] == map[n.i][n.j] + 1) {
      toVisit.push({ i: n.i, j: n.j + 1 })
    }
  }
  return headsNb
}

let count = 0
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (!isStart(i, j)) continue
    count += numberOfTrailheads(i, j)
  }
}

console.log(count)
