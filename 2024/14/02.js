const fs = require('fs')
const { createCanvas } = require('canvas')

const robots = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => {
    const regex = /(-?\d+,-?\d+)/g
    const groups = [...it.matchAll(regex)]
    const p = groups[0][0].split(',').map(that => +that)
    const v = groups[1][0].split(',').map(that => +that)
    return { p: { x: p[0], y: p[1] }, v: { x: v[0], y: v[1] } }
  })

//const I = 7
const I = 103
//const J = 11
const J = 101

const move = robot => {
  robot.p.x = (robot.p.x + robot.v.x) % J
  robot.p.y = (robot.p.y + robot.v.y) % I
  if (robot.p.x < 0) robot.p.x += J
  if (robot.p.y < 0) robot.p.y += I
}

const print = (i) => {
  const canvas = createCanvas(3 * J, 3 * I)
  const ctx = canvas.getContext('2d')
  for (r of robots) {
    ctx.fillStyle = '#0f0'
    ctx.fillRect(3 * r.p.x, 3 * r.p.y, 3, 3)
  }
  const buffer = canvas.toBuffer('image/png')
  fs.writeFileSync('./' + i + '.png', buffer)
}

let i = 0
while (true) {
  // change so that it prints everything
  if (i == 7861) {
    print(i)
    break
  }
  i++
  robots.forEach(r => move(r))
}
