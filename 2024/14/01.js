const fs = require('fs')

const robots = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => {
    const regex = /(-?\d+,-?\d+)/g
    const groups = [...it.matchAll(regex)]
    const p = groups[0][0].split(',').map(that => +that)
    const v = groups[1][0].split(',').map(that => +that)
    return { p: { x: p[0], y: p[1] }, v: { x: v[0], y: v[1] } }
  })

//const I = 7
const I = 103
//const J = 11
const J = 101

const move = robot => {
  robot.p.x = (robot.p.x + robot.v.x) % J
  robot.p.y = (robot.p.y + robot.v.y) % I
  if (robot.p.x < 0) robot.p.x += J
  if (robot.p.y < 0) robot.p.y += I
}

const quadrant = robot => {
  if (robot.p.x < Math.floor(J / 2) && robot.p.y < Math.floor(I / 2)) return 1
  if (robot.p.x > J / 2 && robot.p.y < Math.floor(I / 2)) return 2
  if (robot.p.x < Math.floor(J / 2) && robot.p.y > I / 2) return 3
  if (robot.p.x > J / 2 && robot.p.y > I / 2) return 4
  return -1
}

for (let i = 0; i < 100; i++) {
  robots.forEach(r => move(r))
}

console.log(robots
  .map(it => quadrant(it))
  .filter(it => it > 0)
  .reduce((a, e) => { a[e - 1]++; return a }, [0, 0, 0, 0])
  .reduce((a, b) => a * b, 1)
)


