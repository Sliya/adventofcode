const fs = require('fs')

const disk = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('')
  .map(it => +it)
  .reduce((d, b, i) => {
    d.push({ id: i % 2 == 1 ? -1 : i / 2, size: b })
    return d
  }, [])

const compact = (disk) => {
  let lastFileId = disk.findLast(e => e != -1).id
  while (true) {
    if (lastFileId < 0) break
    const lastFileBlockIdx = disk.findIndex(e => e.id == lastFileId)
    const fileBlock = disk[lastFileBlockIdx]
    const nextFreeBlockIdx = disk.findIndex(e => e.id == -1 && e.size >= fileBlock.size)
    if (nextFreeBlockIdx == -1 || lastFileBlockIdx < nextFreeBlockIdx) {
      lastFileId--
      continue
    }
    const freeBlock = disk[nextFreeBlockIdx]
    if (freeBlock.size > fileBlock.size) {
      disk.splice(nextFreeBlockIdx + 1, 0, { id: -1, size: freeBlock.size - fileBlock.size })
      freeBlock.id = fileBlock.id
      freeBlock.size = fileBlock.size
      fileBlock.id = -1
    } else if (freeBlock.size == fileBlock.size) {
      freeBlock.id = fileBlock.id
      fileBlock.id = -1
    }
    lastFileId--
    for (let i = 1; i < disk.length; i++) {
      if (disk[i].id == disk[i - 1].id) {
        disk[i - 1].size += disk[i].size;
        disk.splice(i, 1)
        i--
      }
    }
  }
  return disk
}

let d = compact(disk)

console.log(
  d
    .map(it => new Array(it.size).fill(it.id))
    .flat()
    .map(it => +it)
    .reduce((acc, e, i) => acc + (e == -1 ? 0 : e) * i, 0)
)
