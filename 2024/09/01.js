const fs = require('fs')

const disk = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('')
  .map(it => +it)
  .reduce((d, b, i) => {
    d.push({ id: i % 2 == 1 ? -1 : i / 2, size: b })
    return d
  }, [])

const compact = (disk) => {
  while (true) {
    const nextFreeBlockIdx = disk.findIndex(e => e.id == -1)
    if (nextFreeBlockIdx == null) break
    const lastFileBlockIdx = disk.findLastIndex(e => e.id != -1)
    if (lastFileBlockIdx < nextFreeBlockIdx) break
    const freeBlock = disk[nextFreeBlockIdx]
    const fileBlock = disk[lastFileBlockIdx]
    if (freeBlock.size > fileBlock.size) {
      disk.splice(lastFileBlockIdx, 1)
      disk.splice(nextFreeBlockIdx + 1, 0, { id: -1, size: freeBlock.size - fileBlock.size })
      freeBlock.id = fileBlock.id
      freeBlock.size = fileBlock.size
    } else if (freeBlock.size == fileBlock.size) {
      disk.splice(lastFileBlockIdx, 1)
      freeBlock.id = fileBlock.id
    } else {
      freeBlock.id = fileBlock.id
      fileBlock.size -= freeBlock.size
    }
  }
  for (let i = 1; i < disk.length; i++) {
    if (disk[i].id == disk[i - 1].id) {
      disk[i - 1].size += disk[i].size;
      disk.splice(i, 1)
      i--
    }
  }
  return disk
}

let d = compact(disk)

console.log(
  d
    .map(it => new Array(it.size).fill(it.id))
    .flat()
    .map(it => +it)
    .reduce((acc, e, i) => acc + (e == -1 ? 0 : e) * i, 0)
)
