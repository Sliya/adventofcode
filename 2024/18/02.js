const fs = require('fs')

const mem = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(',').map(that => +that))
  .map(it => it[0] + ',' + it[1])

const hasPath = (memArr) => {
  let mem = memArr
    .reduce((acc, e) => {
      acc.add(e)
      return acc
    }, new Set())
  const start = { x: 0, y: 0, k: '0,0' }
  const end = { x: 70, y: 70, k: '70,70' }
  const L = 71

  const nextNode = (toVisit, dist) => {
    let iMin = -1
    let dMin = Number.POSITIVE_INFINITY
    toVisit.forEach((n, i) => {
      if ((dist[n.k] ?? Number.POSITIVE_INFINITY) < dMin) {
        dMin = dist[n.k]
        iMin = i
      }
    })
    const node = toVisit[iMin]
    toVisit.splice(iMin, 1)
    return node
  }

  const neighbours = (n) => {
    const ret = []
    if (n.x - 1 >= 0 && !mem.has((n.x - 1) + ',' + n.y)) {
      ret.push({ x: n.x - 1, y: n.y, k: (n.x - 1) + ',' + n.y })
    }
    if (n.x + 1 < L && !mem.has((n.x + 1) + ',' + n.y)) {
      ret.push({ x: n.x + 1, y: n.y, k: (n.x + 1) + ',' + n.y })
    }
    if (n.y - 1 >= 0 && !mem.has(n.x + ',' + (n.y - 1))) {
      ret.push({ x: n.x, y: n.y - 1, k: n.x + ',' + (n.y - 1) })
    }
    if (n.y + 1 < L && !mem.has(n.x + ',' + (n.y + 1))) {
      ret.push({ x: n.x, y: n.y + 1, k: n.x + ',' + (n.y + 1) })
    }
    return ret
  }

  const toVisit = [start]
  const dist = {}
  dist[start.k] = 0
  const prec = {}
  while (toVisit.length > 0) {
    const n = nextNode(toVisit, dist)
    neighbours(n).forEach(ngb => {
      if (dist[n.k] + 1 < (dist[ngb.k] ?? Number.POSITIVE_INFINITY)) {
        dist[ngb.k] = dist[n.k] + 1
        prec[ngb.k] = n.k
        toVisit.push(ngb)
      }
    })
  }

  return prec[end.k] != null

}
let i = 1024
while (hasPath(mem.slice(0, i++))) { }
console.log(mem[i - 2])
