const fs = require('fs')

const locations = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it
    .split('   ')
    .map(i => +i)
  )
  .reduce((acc, e) => {
    acc[0].push(e[0])
    acc[1].push(e[1])
    return acc;
  }, [[], []])
  .map(it => it.toSorted())

console.log(
  locations[0]
    .map((e, i) => Math.abs(e - locations[1][i]))
    .reduce((a, b) => a + b)
)
