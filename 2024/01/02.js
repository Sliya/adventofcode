const fs = require('fs')

const locations = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it
    .split('   ')
    .map(i => +i)
  )
  .reduce((acc, e) => {
    acc[0].push(e[0])
    acc[1].push(e[1])
    return acc
  }, [[], []])
  .map(it => it.toSorted())

console.log(
  locations[0]
    .map((e, i) => [e, locations[1].filter(it => it == e).length])
    .map(it => it[0] * it[1])
    .reduce((a, b) => a + b)
)
