// Should run with --stack_size=1200

const fs = require('fs')

const map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

let start = {}
let end = {}

for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == 'S') {
      start.x = j
      start.y = i
    } else if (map[i][j] == 'E') {
      end.x = j
      end.y = i
    }
  }
}

const path = map.map(it => it.map(_ => Number.POSITIVE_INFINITY))

const visited = new Set()
const dist = (i, j) => {
  if (i == end.y && j == end.x) {
    path[i][j] = 0
    return 0
  }
  visited.add(i + ',' + j)
  if (i - 1 >= 0 && map[i - 1][j] != '#' && !visited.has((i - 1) + ',' + j)) {
    path[i][j] = dist(i - 1, j) + 1
  }
  if (i + 1 < map.length && map[i + 1][j] != '#' && !visited.has((i + 1) + ',' + j)) {
    path[i][j] = dist(i + 1, j) + 1
  }
  if (j - 1 >= 0 && map[i][j - 1] != '#' && !visited.has(i + ',' + (j - 1))) {
    path[i][j] = dist(i, j - 1) + 1
  }
  if (j + 1 < map[0].length && map[i][j + 1] != '#' && !visited.has(i + ',' + (j + 1))) {
    path[i][j] = dist(i, j + 1) + 1
  }
  return path[i][j]
}

dist(start.y, start.x)

const shortcuts = {}
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (path[i][j] == Number.POSITIVE_INFINITY) continue
    if (i - 2 >= 0) {
      const saved = path[i][j] - path[i - 2][j] - 2
      if (shortcuts[saved] == null)
        shortcuts[saved] = 0
      shortcuts[saved]++
    }
    if (i + 2 < map.length) {
      const saved = path[i][j] - path[i + 2][j] - 2
      if (shortcuts[saved] == null)
        shortcuts[saved] = 0
      shortcuts[saved]++
    }
    if (j - 2 >= 0) {
      const saved = path[i][j] - path[i][j - 2] - 2
      if (shortcuts[saved] == null)
        shortcuts[saved] = 0
      shortcuts[saved]++
    }
    if (j + 2 < map[0].length) {
      const saved = path[i][j] - path[i][j + 2] - 2
      if (shortcuts[saved] == null)
        shortcuts[saved] = 0
      shortcuts[saved]++
    }
  }
}

console.log([...Object.keys(shortcuts)].map(it => +it).filter(it => it >= 100).map(it => shortcuts[it]).reduce((a, b) => a + b, 0))
