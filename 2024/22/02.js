const fs = require('fs')

let secrets = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => BigInt(it))

const mix = (a, b) => a ^ b
const prune = n => n % 16777216n

const step1 = n => prune(mix(n, n * 64n))
const step2 = n => prune(mix(n, n / 32n))
const step3 = n => prune(mix(n, n * 2048n))
const next = n => step3(step2(step1(n)))

const deltas = secrets.map(_ => [])
const prices = secrets.map(n => [n % 10n])
for (let i = 0; i < 2000; i++) {
  const nextSecrets = secrets.map(next)
  nextSecrets.forEach((s, i) => prices[i].push(s % 10n))
  secrets.map((s, i) => (nextSecrets[i] % 10n) - (s % 10n))
    .forEach((d, i) => deltas[i].push(d))
  secrets = nextSecrets
}

const sequenceToPrice = []
const keys = new Set()
for (let k = 0; k < deltas.length; k++) {
  const delta = deltas[k]
  const map = {}
  sequenceToPrice.push(map)
  for (let i = 0; i <= delta.length - 4; i++) {
    const key = delta.slice(i, i + 4).join('')
    keys.add(key)
    if (key in map) continue
    map[key] = prices[k][i + 4]
  }
}

console.log(
  keys.values()
    .map(it => sequenceToPrice.map(m => m[it] ?? 0n).reduce((a, b) => a + b, 0n))
    .reduce((a, b) => a > b ? a : b, -Infinity)
)
