const fs = require('fs')

let secrets = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => BigInt(it))

const mix = (a, b) => a ^ b
const prune = n => n % 16777216n

const step1 = n => prune(mix(n, n * 64n))
const step2 = n => prune(mix(n, n / 32n))
const step3 = n => prune(mix(n, n * 2048n))
const next = n => step3(step2(step1(n)))

for (let i = 0; i < 2000; i++) {
  secrets = secrets.map(next)
}
console.log(secrets.reduce((a, b) => a + b, 0n))
