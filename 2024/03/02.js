const fs = require('fs')

const memory = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()

const regex = /mul\((\d{1,3},\d{1,3})\)|do\(\)|don't\(\)/g
let sum = 0
let enabled = true

for (const match of memory.matchAll(regex)) {
  if (match[0] == "do()") enabled = true
  if (match[0] == "don't()") enabled = false
  if (enabled && match[0].startsWith("mul"))
    sum += match[1].split(',').map(it => +it).reduce((a, b) => a * b, 1)
}

console.log(sum)
