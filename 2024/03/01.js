const fs = require('fs')

const memory = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()

const regex = /mul\((\d{1,3},\d{1,3})\)/g
let sum = 0

for (const match of memory.matchAll(regex)) {
  sum += match[1].split(',').map(it => +it).reduce((a, b) => a * b, 1)
}

console.log(sum)
