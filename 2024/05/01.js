const fs = require('fs')


let [rules, updates] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')

rules = rules.split('\n').map(it => it.split('|').map(that => +that))
updates = updates.split('\n').map(it => it.split(',').map(that => +that))

const precedence = rules.reduce((acc, e) => {
  if (acc[e[1]] == null) {
    acc[e[1]] = []
  }
  acc[e[1]].push(e[0])
  return acc
}, {})

const printable = (update) => {
  return update.every((e, i) => (precedence[e] ?? []).every(p => update.slice(0, i).includes(p) || !update.includes(p)))
}

console.log(
  updates.filter(it => printable(it))
    .map(it => it[Math.floor(it.length / 2)])
    .reduce((a, b) => a + b, 0)
)
