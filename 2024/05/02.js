const fs = require('fs')

let [rules, updates] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')

rules = rules.split('\n').map(it => it.split('|').map(that => +that))
updates = updates.split('\n').map(it => it.split(',').map(that => +that))

const precedence = rules.reduce((acc, e) => {
  if (acc[e[1]] == null) {
    acc[e[1]] = []
  }
  acc[e[1]].push(e[0])
  return acc
}, {})

const check = (update, prec, i) => prec.every(p => update.slice(0, i).includes(p) || !update.includes(p))

const printable = (update) => {
  return update.every((e, i) => check(update, precedence[e] ?? [], i))
}

const reorder = (update) => {
  for (let i = 0; i < update.length; i++) {
    const e = update[i]
    const pres = precedence[e] ?? []
    if (check(update, pres, i)) continue
    const other = pres.find(p => !update.slice(0, i).includes(p) && update.includes(p))
    const idxOther = update.indexOf(other)
    update[i] = other
    update[idxOther] = e
    i--
  }
  return update
}

console.log(
  updates.filter(it => !printable(it))
    .map(reorder)
    .map(it => it[Math.floor(it.length / 2)])
    .reduce((a, b) => a + b, 0)
)
