const fs = require('fs')

const tinks = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')
  .map(it => it.split('\n').map(that => that.split('')))

const transpose = (matrix) => {
  return matrix.reduce((prev, next) => next.map((item, i) =>
    (prev[i] || []).concat(next[i])
  ), []);
}

const locks = tinks.filter(it => it[0].every(that => that == '#') && it[it.length - 1].every(that => that == '.')).map(it => transpose(it))
const keys = tinks.filter(it => it[0].every(that => that == '.') && it[it.length - 1].every(that => that == '#')).map(it => transpose(it))

const lockCodes = locks
  .map(it => it.map(that => that.length - that.indexOf('.')))
const keyCodes = keys.map(it => it.map(that => that.length - that.indexOf('#')))

console.log(
  lockCodes.map(l => keyCodes.filter(k =>
    k.every((n, i) => n <= l[i])
  ).length)
    .reduce((a, b) => a + b, 0)
)
