const fs = require('fs')

const map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

const I = map.length
const J = map[0].length
const antennas = {}
for (let i = 0; i < I; i++) {
  for (let j = 0; j < J; j++) {
    const p = map[i][j]
    if (p != '.') {
      if (antennas[p] == null) {
        antennas[p] = []
      }
      antennas[p].push([i, j])
    }
  }
}

const isInside = (i, j) => i >= 0 && i < I && j >= 0 && j < J

const computeAntinodes = (ants) => {
  const nodes = new Set()
  for (let i = 0; i < ants.length; i++) {
    for (let j = i + 1; j < ants.length; j++) {
      const a = ants[i]
      const b = ants[j]
      const di = a[0] - b[0]
      const dj = a[1] - b[1]
      let ni = a[0]
      let nj = a[1]
      nodes.add(`${ni},${nj}`)
      while (isInside(ni + di, nj + dj)) {
        nodes.add(`${ni + di},${nj + dj}`)
        ni = ni + di
        nj = nj + dj
      }
      ni = b[0]
      nj = b[1]
      nodes.add(`${ni},${nj}`)
      while (isInside(ni - di, nj - dj)) {
        nodes.add(`${ni - di},${nj - dj}`)
        ni = ni - di
        nj = nj - dj
      }
    }
  }
  return nodes
}

const antinodes = new Set()
for (const freq in antennas) {
  computeAntinodes(antennas[freq]).forEach(it => antinodes.add(it))
}

console.log(antinodes.size)


