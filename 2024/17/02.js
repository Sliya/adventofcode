const fs = require('fs')

const program = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')[1]
  .split(': ')[1]
  .split(',')
  .map(it => +it)

const guess = (prog, g) => {
  if (prog.length == 0) return g
  for (let i = 0n; i < 8n; i++) {
    let a = (g << 3n) + i
    let b = a % 8n
    b = b ^ 2n
    let c = a / (2n ** b)
    b = b ^ c
    b = b ^ 3n
    if (b % 8n == prog[prog.length - 1]) {
      let next = guess(prog.slice(0, prog.length - 1), a)
      if (next == undefined) continue
      return next
    }
  }
}

console.log(guess(program, 0n))

