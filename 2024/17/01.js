const fs = require('fs')
const { it } = require('node:test')

const [registersStr, programStr] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')

let [A, B, C] = registersStr
  .split('\n')
  .map(it => it.split(': ')[1])
  .map(it => +it)

const program = programStr
  .split(': ')[1]
  .split(',')
  .map(it => +it)

const comboVal = o => {
  if (o >= 0 && o <= 3) return o
  if (o == 4) return A
  if (o == 5) return B
  if (o == 6) returnC
  throw "Invalid combo operand : " + o
}

let ip = 0
let consoleOut = []

const ops = [
  o => {
    A = Math.trunc(A / (2 ** comboVal(o)))
  },
  o => {
    B = B ^ o
  },
  o => {
    B = comboVal(o) % 8
  },
  o => {
    if (A == 0) return;
    ip = o - 2 // Because pointer should not be increased by 2 after a jump
  },
  o => {
    B = B ^ C
  },
  o => {
    consoleOut.push(comboVal(o) % 8)
  },
  o => {
    B = Math.trunc(A / Math.pow(2, comboVal(o)))
  },
  o => {
    C = Math.trunc(A / Math.pow(2, comboVal(o)))
  }
]

while (ip < program.length) {
  ops[program[ip]](program[ip + 1])
  ip += 2
}

console.log(consoleOut.join(','))
