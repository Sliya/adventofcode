const fs = require('fs')

const map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it
    .split('')
  )

let posI = 0
let posJ = 0
let dir = "UP"

for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == '^') {
      posI = i
      posJ = j
    }
  }
}

let visited = new Set()
visited.add(posI + ',' + posJ)

try {
  while (posI >= 0 && posI < map.length && posJ >= 0 && posJ < map[0].length) {
    visited.add(posI + ',' + posJ)
    switch (dir) {
      case "UP":
        if (map[posI - 1][posJ] == '#') {
          dir = "RIGHT"
        } else {
          posI--
        }
        break
      case "RIGHT":
        if (map[posI][posJ + 1] == '#') {
          dir = "DOWN"
        } else {
          posJ++
        }
        break
      case "DOWN":
        if (map[posI + 1][posJ] == '#') {
          dir = "LEFT"
        } else {
          posI++
        }
        break
      case "LEFT":
        if (map[posI][posJ - 1] == '#') {
          dir = "UP"
        } else {
          posJ--
        }
        break
    }
  }
} catch (e) { }

console.log(visited.size)
