const fs = require('fs')

const map0 = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it
    .split('')
  )

let posI0 = 0
let posJ0 = 0
for (let i = 0; i < map0.length; i++) {
  for (let j = 0; j < map0[0].length; j++) {
    if (map0[i][j] == '^') {
      posI0 = i
      posJ0 = j
    }
  }
}

const computePath = (map) => {
  let visited = new Set()
  let dir = "UP"
  let posI = posI0
  let posJ = posJ0

  try {
    while (posI >= 0 && posI < map.length && posJ >= 0 && posJ < map[0].length) {
      if (visited.has(posI + ',' + posJ + ',' + dir)) {
        return true
      }
      visited.add(posI + ',' + posJ + ',' + dir)
      switch (dir) {
        case "UP":
          if (map[posI - 1][posJ] == '#') {
            dir = "RIGHT"
          } else {
            posI--
          }
          break
        case "RIGHT":
          if (map[posI][posJ + 1] == '#') {
            dir = "DOWN"
          } else {
            posJ++
          }
          break
        case "DOWN":
          if (map[posI + 1][posJ] == '#') {
            dir = "LEFT"
          } else {
            posI++
          }
          break
        case "LEFT":
          if (map[posI][posJ - 1] == '#') {
            dir = "UP"
          } else {
            posJ--
          }
          break
      }
    }
  } catch (e) { }
  return new Set(
    [...visited.values()].map(it => it.split(',').slice(0, 2).join(','))
  )
}

const path = computePath(map0, posI0, posJ0)
path.delete(posI0 + ',' + posJ0)
console.log(
  [...path.values()]
    .map(it => it.split(',').map(that => +that))
    .map(it => {
      let map = map0.map(i => i.map(j => j))
      map[it[0]][it[1]] = '#'
      return map
    })
    .filter(it => computePath(it) === true)
    .length
)
