const fs = require('fs')

const delta = arr => arr.map((e, i) => i < arr.length - 1 ? e - arr[i + 1] : null)
  .filter(it => it != null)

const safe = d => d.every(it => it >= 1 && it <= 3) || d.every(it => it <= -1 && it >= -3)

const isSafe = arr => {
  if (safe(delta(arr))) {
    return true
  }
  for (let i = 0; i < arr.length; i++) {
    if (safe(delta(arr.filter((_, j) => j != i)))) {
      return true
    }
  }
  return false
}

console.log(fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it
    .split(' ')
    .map(i => +i)
  )
  .filter(it => isSafe(it))
  .length
)
