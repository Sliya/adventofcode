const fs = require('fs')

const isSafe = arr => {
  const delta = arr.map((e, i) => i < arr.length - 1 ? e - arr[i + 1] : null)
    .filter(it => it != null)
  return delta.every(it => it >= 1 && it <= 3) || delta.every(it => it <= -1 && it >= -3)
}

console.log(fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it
    .split(' ')
    .map(i => +i)
  )
  .filter(it => isSafe(it))
  .length
)
