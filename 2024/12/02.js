const fs = require('fs')

let map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

const computeSides = (perimeters) => {
  perimeters.sort((a, b) => {
    if (a.d == "H" && b.d == "V") return -1
    if (a.d == "V" && b.d == "H") return 1
    if (a.d == "H" && a.i > b.i) return -1
    if (a.d == "H" && a.i == b.i && a.j < b.j) return -1
    if (a.d == "V" && a.j > b.j) return -1
    if (a.d == "V" && a.j == b.j && a.i < b.i) return -1

  })
  let sides = 1
  // on change de côté si on change d'orientation ou si il y a un saut de plus de 1
  for (let i = 1; i < perimeters.length; i++) {
    if (perimeters[i - 1].d != perimeters[i].d) {
      sides++
      continue
    }
    if (perimeters[i].d == 'H' && perimeters[i - 1].i != perimeters[i].i) {
      sides++
      continue
    }
    if (perimeters[i].d == 'V' && perimeters[i - 1].j != perimeters[i].j) {
      sides++
      continue
    }
    if (perimeters[i].d == 'H' && perimeters[i - 1].j != perimeters[i].j - 1) {
      sides++
      continue
    }
    if (perimeters[i].d == 'V' && perimeters[i - 1].i != perimeters[i].i - 1) {
      sides++
      continue
    }
  }
  return sides
}


const seen = new Set()
let price = 0
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    const key = [i, j].join(',')
    let perimeters = []
    let area = 0
    if (seen.has(key)) continue
    const toBeSeen = [{ i, j }]

    while (toBeSeen.length > 0) {
      const n = toBeSeen.pop()
      if (seen.has([n.i, n.j].join(','))) continue
      area++
      seen.add([n.i, n.j].join(','))
      if (n.i - 1 >= 0 && !seen.has([n.i - 1, n.j].join(',')) && map[n.i - 1][n.j] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i - 1, j: n.j })
      } else if (n.i - 1 < 0 || map[n.i - 1][n.j] != map[n.i][n.j]) {
        // on met 1.1 pour différencier les barrières qui sont au même endroit à gauche et à droite
        // A B A 
        // A|-|A
        perimeters.push({ i: n.i - 1.1, j: n.j, d: 'H' })
      }

      if (n.i + 1 < map.length && !seen.has([n.i + 1, n.j].join(',')) && map[n.i + 1][n.j] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i + 1, j: n.j })
      } else if (n.i + 1 >= map.length || map[n.i + 1][n.j] != map[n.i][n.j]) {
        perimeters.push({ i: n.i + 1.1, j: n.j, d: 'H' })
      }

      if (n.j + 1 < map[0].length && !seen.has([n.i, n.j + 1].join(',')) && map[n.i][n.j + 1] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i, j: n.j + 1 })
      } else if (n.j + 1 >= map[0].length || map[n.i][n.j + 1] != map[n.i][n.j]) {
        perimeters.push({ i: n.i, j: n.j + 1.1, d: 'V' })
      }

      if (n.j - 1 >= 0 && !seen.has([n.i, n.j - 1].join(',')) && map[n.i][n.j - 1] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i, j: n.j - 1 })
      } else if (n.j - 1 < 0 || map[n.i][n.j - 1] != map[n.i][n.j]) {
        perimeters.push({ i: n.i, j: n.j - 1.1, d: 'V' })
      }
    }
    price += area * computeSides(perimeters)
  }
}

console.log(price)
