const fs = require('fs')

let map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

const seen = new Set()
let price = 0
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    const key = [i, j].join(',')
    let perimeter = 0
    let area = 0
    if (seen.has(key)) continue
    const toBeSeen = [{ i, j }]
    while (toBeSeen.length > 0) {
      const n = toBeSeen.pop()
      if (seen.has([n.i, n.j].join(','))) continue
      area++
      seen.add([n.i, n.j].join(','))
      if (n.i - 1 >= 0 && !seen.has([n.i - 1, n.j].join(',')) && map[n.i - 1][n.j] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i - 1, j: n.j })
      } else if (n.i - 1 < 0 || map[n.i - 1][n.j] != map[n.i][n.j]) {
        perimeter++
      }
      if (n.i + 1 < map.length && !seen.has([n.i + 1, n.j].join(',')) && map[n.i + 1][n.j] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i + 1, j: n.j })
      } else if (n.i + 1 >= map.length || map[n.i + 1][n.j] != map[n.i][n.j]) {
        perimeter++
      }
      if (n.j + 1 < map[0].length && !seen.has([n.i, n.j + 1].join(',')) && map[n.i][n.j + 1] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i, j: n.j + 1 })
      } else if (n.j + 1 >= map[0].length || map[n.i][n.j + 1] != map[n.i][n.j]) {
        perimeter++
      }
      if (n.j - 1 >= 0 && !seen.has([n.i, n.j - 1].join(',')) && map[n.i][n.j - 1] == map[n.i][n.j]) {
        toBeSeen.push({ i: n.i, j: n.j - 1 })
      } else if (n.j - 1 < 0 || map[n.i][n.j - 1] != map[n.i][n.j]) {
        perimeter++
      }
    }
    price += area * perimeter
  }
}
console.log(price)
