const fs = require('fs')

let [map, dirs] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')
  .map(it => it.split('\n').map(that => that.split('')))

let x = 0, y = 0
search:
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == '@') {
      x = j
      y = i
      //map[i][j] = '.'
      break search
    }
  }
}

dirs = dirs.flat()

const canMove = (dir) => {
  if (dir == '^') {
    for (let i = y - 1; i >= 0; i--) {
      if (map[i][x] == '.') return true
      if (map[i][x] == '#') return false
    }
  }
  if (dir == 'v') {
    for (let i = y + 1; i < map.length; i++) {
      if (map[i][x] == '.') return true
      if (map[i][x] == '#') return false
    }
  }
  if (dir == '<') {
    for (let j = x - 1; j >= 0; j--) {
      if (map[y][j] == '.') return true
      if (map[y][j] == '#') return false
    }
  }
  if (dir == '>') {
    for (let j = x + 1; j < map[0].length; j++) {
      if (map[y][j] == '.') return true
      if (map[y][j] == '#') return false
    }
  }
}

const move = (dir) => {
  if (dir == '^') {
    for (let i = y - 1; i >= 0; i--) {
      if (map[i][x] == '.') {
        for (let ii = i; ii < y; ii++) {
          map[ii][x] = map[ii + 1][x]
        }
        break
      }
    }
    map[y][x] = '.'
    y--
  }
  if (dir == 'v') {
    for (let i = y + 1; i < map.length; i++) {
      if (map[i][x] == '.') {
        for (let ii = i; ii > y; ii--) {
          map[ii][x] = map[ii - 1][x]
        }
        break
      }
    }
    map[y][x] = '.'
    y++
  }
  if (dir == '<') {
    for (let j = x - 1; j >= 0; j--) {
      if (map[y][j] == '.') {
        for (let jj = j; jj < x; jj++) {
          map[y][jj] = map[y][jj + 1]
        }
        break
      }
    }
    map[y][x] = '.'
    x--
  }
  if (dir == '>') {
    for (let j = x + 1; j < map[0].length; j++) {
      if (map[y][j] == '.') {
        for (let jj = j; jj > x; jj--) {
          map[y][jj] = map[y][jj - 1]
        }
        break
      }
    }
    map[y][x] = '.'
    x++
  }
}

dirs.forEach(dir => {
  if (!canMove(dir)) return
  move(dir)
})

let count = 0

for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == 'O') {
      count += 100 * i + j
    }
  }
}


//console.log(map.map(it => it.join('')).join('\n'))
console.log(count)
