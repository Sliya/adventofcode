const fs = require('fs')

let [map, dirs] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')
  .map(it => it.split('\n').map(that => that.split('')))

map = map.map(it =>
  it.map(that => {
    if (that == '#') return ['#', '#']
    if (that == 'O') return ['[', ']']
    if (that == '.') return ['.', '.']
    if (that == '@') return ['@', '.']
  }).flat()
)

let x = 0, y = 0
search:
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == '@') {
      x = j
      y = i
      map[i][j] = '.'
      break search
    }
  }
}

dirs = dirs.flat()

const canMove = (dir, i, j) => {
  if (map[i][j] == '#') return false
  if (map[i][j] == '.') return true
  if (dir == '^') {
    if (map[i][j] == '[') return canMove(dir, i - 1, j) && canMove(dir, i - 1, j + 1)
    return canMove(dir, i - 1, j) && canMove(dir, i - 1, j - 1)
  }
  if (dir == 'v') {
    if (map[i][j] == '[') return canMove(dir, i + 1, j) && canMove(dir, i + 1, j + 1)
    return canMove(dir, i + 1, j) && canMove(dir, i + 1, j - 1)
  }
  if (dir == '<') {
    return canMove(dir, i, j - 1)
  }
  if (dir == '>') {
    return canMove(dir, i, j + 1)
  }
}

const move = (dir, i, j) => {
  if (map[i][j] == '.') return
  if (dir == '^') {
    move(dir, i - 1, j)
    if (map[i][j] == '[') {
      move(dir, i - 1, j + 1)
      map[i - 1][j + 1] = map[i][j + 1]
    }
    if (map[i][j] == ']') {
      move(dir, i - 1, j - 1)
      map[i - 1][j - 1] = map[i][j - 1]
    }
    if (map[i][j] == '[')
      map[i][j + 1] = '.'
    if (map[i][j] == ']')
      map[i][j - 1] = '.'
    map[i - 1][j] = map[i][j]
  }
  if (dir == 'v') {
    move(dir, i + 1, j)
    if (map[i][j] == '[') {
      move(dir, i + 1, j + 1)
      map[i + 1][j + 1] = map[i][j + 1]
    }
    if (map[i][j] == ']') {
      move(dir, i + 1, j - 1)
      map[i + 1][j - 1] = map[i][j - 1]
    }
    if (map[i][j] == '[')
      map[i][j + 1] = '.'
    if (map[i][j] == ']')
      map[i][j - 1] = '.'
    map[i + 1][j] = map[i][j]
  }
  if (dir == '<') {
    move(dir, i, j - 1)
    map[i][j - 1] = map[i][j]
  }
  if (dir == '>') {
    move(dir, i, j + 1)
    map[i][j + 1] = map[i][j]
  }
  map[i][j] = '.'
}

//console.log(map.map(it => it.join('')).join('\n'))
dirs.forEach(dir => {
  if (dir == '^') {
    if (!canMove(dir, y - 1, x)) return
    move(dir, y - 1, x)
    y--
  }
  if (dir == 'v') {
    if (!canMove(dir, y + 1, x)) return
    move(dir, y + 1, x)
    y++
  }
  if (dir == '<') {
    if (!canMove(dir, y, x - 1)) return
    move(dir, y, x - 1)
    x--
  }
  if (dir == '>') {
    if (!canMove(dir, y, x + 1)) return
    move(dir, y, x + 1)
    x++
  }
})

let count = 0

for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == '[') {
      count += 100 * i + j
    }
  }
}

console.log(count)
