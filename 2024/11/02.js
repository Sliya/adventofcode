const fs = require('fs')

let stones = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split(' ')
  .map(it => +it)

const blink = stone => {
  if (stone == 0) return [1]
  const length = ('' + stone).length
  if (length % 2 == 0) return [+(('' + stone).substring(0, length / 2)), +(('' + stone).substring(length / 2))]
  return [stone * 2024]
}

const mem = {}
const count = (stone, nb) => {
  if (nb == 0) return 1
  const key = `${stone},${nb}`
  if (mem[key] != null) return mem[key]
  mem[key] = blink(stone).map(it => count(it, nb - 1)).reduce((a, b) => a + b, 0)
  return mem[key]
}

console.log(
  stones.map(it => count(it, 75)).reduce((a, b) => a + b, 0)
)
