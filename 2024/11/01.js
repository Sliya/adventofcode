const fs = require('fs')

let stones = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split(' ')
  .map(it => +it)

const blink = stone => {
  if (stone == 0) return [1]
  const length = ('' + stone).length
  if (length % 2 == 0) return [+(('' + stone).substring(0, length / 2)), +(('' + stone).substring(length / 2))]
  return [stone * 2024]
}

for (let i = 0; i < 25; i++) {
  stones = stones.map(blink).flat()
}
console.log(stones.length)
