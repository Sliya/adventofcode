const fs = require('fs')

const [wiresStr, gatesStr] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')

const wires = wiresStr.split('\n')
  .map(it => it.split(': '))
  .reduce((acc, e) => { acc[e[0]] = +e[1]; return acc }, {})

const gates = gatesStr.split('\n')
  .map(it => it.split(' -> '))
  .map(it => {
    const [i1, op, i2] = it[0].split(' ')
    return { in: [i1, i2], op, out: it[1], waiting: true }
  })

const compute = gate => {
  if (gate.op == 'AND') wires[gate.out] = wires[gate.in[0]] & wires[gate.in[1]]
  if (gate.op == 'OR') wires[gate.out] = wires[gate.in[0]] | wires[gate.in[1]]
  if (gate.op == 'XOR') wires[gate.out] = wires[gate.in[0]] ^ wires[gate.in[1]]
  gate.waiting = false
}

while (gates.some(it => it.waiting)) {
  gates.filter(it => it.waiting)
    .filter(it => it.in[0] in wires && it.in[1] in wires)
    .forEach(it => compute(it))
}

console.log(
  [...Object.keys(wires)
    .filter(it => it.startsWith('z'))]
    .toSorted()
    .toReversed()
    .map(it => wires[it])
    .reduce((acc, e) => BigInt(e) + (acc << 1n), 0n)
)

