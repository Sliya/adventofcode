const fs = require('fs')

const map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

let start = {}, end = {}
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == 'S') {
      start.y = i
      start.x = j
      start.dir = 'E'
      start.k = i + ',' + j + ',E'
    }
    if (map[i][j] == 'E') {
      end.y = i
      end.x = j
      end.k = i + ',' + j
    }
  }
}

const neighbours = (y, x, dir) => {
  const n = []
  if (y - 1 >= 0 && map[y - 1][x] != '#') {
    n.push({ y: y - 1, x, k: (y - 1) + ',' + x + ',N', cost: dir == 'N' ? 1 : dir == 'S' ? 2001 : 1001 })
  }
  if (y + 1 < map.length && map[y + 1][x] != '#') {
    n.push({ y: y + 1, x, k: (y + 1) + ',' + x + ',S', cost: dir == 'S' ? 1 : dir == 'N' ? 2001 : 1001 })
  }
  if (x - 1 >= 0 && map[y][x - 1] != '#') {
    n.push({ y, x: x - 1, k: y + ',' + (x - 1) + ',W', cost: dir == 'W' ? 1 : dir == 'E' ? 2001 : 1001 })
  }
  if (x + 1 < map[0].length && map[y][x + 1] != '#') {
    n.push({ y, x: x + 1, k: y + ',' + (x + 1) + ',E', cost: dir == 'E' ? 1 : dir == 'W' ? 2001 : 1001 })
  }
  return n
}

const nextNode = (toVisit, dist) => {
  let dMin = Number.POSITIVE_INFINITY
  let iMin = -1
  toVisit.forEach((e, i) => {
    if (dist[e.k] < dMin) {
      dMin = dist[e.k]
      iMin = i
    }
  })
  const ret = toVisit[iMin]
  toVisit.splice(iMin, 1)
  return ret
}

const nbOfTiles = () => {
  const toVisit = [{ ...start, dir: 'E' }]
  const prec = {}
  const dist = {}
  const visited = new Set()
  dist[start.k] = 0
  while (toVisit.length > 0) {
    const n = nextNode(toVisit, dist)
    if (visited.has(n.k)) continue
    visited.add(n.k)
    const nNeighbours = neighbours(n.y, n.x, n.dir)
    nNeighbours.forEach(ngb => {
      if ((dist[ngb.k] ?? Number.POSITIVE_INFINITY) >= dist[n.k] + ngb.cost) {
        dist[ngb.k] = dist[n.k] + ngb.cost
        if (prec[ngb.k] == null) prec[ngb.k] = []
        prec[ngb.k].push(n)
        let dir = 'E'
        if (ngb.x == n.x + 1) dir = 'E'
        if (ngb.x == n.x - 1) dir = 'W'
        if (ngb.y == n.y + 1) dir = 'S'
        if (ngb.y == n.y - 1) dir = 'N'
        toVisit.push({ ...ngb, dir })

      }
    })
  }
  // Change the dist to the answer of part 1
  const p = [...Object.entries(prec)].filter(it => it[0].startsWith(end.k) && dist[it[0]] == 114476).map(it => it[1]).flat()
  console.log(p)
  const score = new Set()
  while (p.length > 0) {
    const n = p.pop()
    score.add(n.y + ',' + n.x)
    if (prec[n.k] == null) continue
    prec[n.k].forEach(it => p.push(it))
  }
  return score.size + 1
}

console.log(nbOfTiles())
