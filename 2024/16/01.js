const fs = require('fs')

const map = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

let start = {}, end = {}
for (let i = 0; i < map.length; i++) {
  for (let j = 0; j < map[0].length; j++) {
    if (map[i][j] == 'S') {
      start.y = i
      start.x = j
      start.k = i + ',' + j
    }
    if (map[i][j] == 'E') {
      end.y = i
      end.x = j
      end.k = i + ',' + j
    }
  }
}

const cache = {}
const neighbours = (y, x) => {
  const k = y + ',' + x
  if (cache[k] == null) {
    const n = []
    if (y - 1 >= 0 && map[y - 1][x] != '#') {
      n.push({ y: y - 1, x, k: (y - 1) + ',' + x })
    }
    if (y + 1 < map.length && map[y + 1][x] != '#') {
      n.push({ y: y + 1, x, k: (y + 1) + ',' + x })
    }
    if (x - 1 >= 0 && map[y][x - 1] != '#') {
      n.push({ y, x: x - 1, k: y + ',' + (x - 1) })
    }
    if (x + 1 < map[0].length && map[y][x + 1] != '#') {
      n.push({ y, x: x + 1, k: y + ',' + (x + 1) })
    }
    cache[k] = n
  }
  return cache[k]
}

const nextNode = (toVisit, dist) => {
  let dMin = Number.POSITIVE_INFINITY
  let iMin = -1
  toVisit.forEach((e, i) => {
    if (dist[e.k] < dMin) {
      dMin = dist[e.k]
      iMin = i
    }
  })
  const ret = toVisit[iMin]
  toVisit.splice(iMin, 1)
  return ret
}

const shortestPath = () => {
  const toVisit = [{ ...start, dir: 'E' }]
  const prec = {}
  const dist = {}
  dist[start.k] = 0
  while (toVisit.length > 0) {
    const n = nextNode(toVisit, dist)
    const nNeighbours = neighbours(n.y, n.x)
    nNeighbours.forEach(ngb => {
      let cost = 1
      if (n.dir == 'E' && ngb.x != n.x + 1) cost += 1000
      if (n.dir == 'W' && ngb.x != n.x - 1) cost += 1000
      if (n.dir == 'S' && ngb.y != n.y + 1) cost += 1000
      if (n.dir == 'N' && ngb.y != n.y - 1) cost += 1000
      if ((dist[ngb.k] ?? Number.POSITIVE_INFINITY) > dist[n.k] + cost) {
        dist[ngb.k] = dist[n.k] + cost
        prec[ngb.k] = n
        let dir = 'E'
        if (ngb.x == n.x + 1) dir = 'E'
        if (ngb.x == n.x - 1) dir = 'W'
        if (ngb.y == n.y + 1) dir = 'S'
        if (ngb.y == n.y - 1) dir = 'N'
        toVisit.push({ ...ngb, dir })

      }
    })
  }
  const p = []
  let n = end
  while (n != null) {
    p.push(n)
    n = prec[n.k]
  }
  return { path: p.toReversed(), score: dist[end.k] }
}

const { score } = shortestPath()

console.log(score)
