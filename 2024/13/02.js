const fs = require('fs')

console.log(
  fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
  })
    .trim()
    .split('\n\n')
    .map(it => {
      const lines = it.split('\n')
      const regex = /(\d+)/g
      const m = lines.map(that => {
        const groups = [...that.matchAll(regex)]
        return { x: BigInt(groups[0][0]), y: BigInt(groups[1][0]) }
      })
      return {
        a: m[0], b: m[1], price: { x: 10000000000000n + m[2].x, y: 10000000000000n + m[2].y }
      }
    })
    .map(it => {
      const b = (it.price.y * it.a.x - it.a.y * it.price.x) / (it.b.y * it.a.x - it.a.y * it.b.x)
      const a = (it.price.x - it.b.x * b) / it.a.x
      return { a, b, tokens: 3n * a + b, machine: it }
    })
    .filter(it =>
      it.a * it.machine.a.x + it.b * it.machine.b.x == it.machine.price.x
      &&
      it.a * it.machine.a.y + it.b * it.machine.b.y == it.machine.price.y
    )
    .map(it => it.tokens)
    .reduce((a, b) => a + b, 0n)
)


