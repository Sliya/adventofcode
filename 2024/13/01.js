const fs = require('fs')

console.log(
  fs.readFileSync('./input', {
    encoding: 'utf8',
    flag: 'r'
  })
    .trim()
    .split('\n\n')
    .map(it => {
      const lines = it.split('\n')
      const regex = /(\d+)/g
      const m = lines.map(that => {
        const groups = [...that.matchAll(regex)]
        return { x: +groups[0][0], y: +groups[1][0] }
      })
      return { a: m[0], b: m[1], price: m[2] }
    })
    .map(it => {
      const b = (it.price.y - (it.a.y * it.price.x / it.a.x)) / (it.b.y - it.a.y * it.b.x / it.a.x)
      const a = it.price.x / it.a.x - (it.b.x / it.a.x) * b
      return { a, b, tokens: 3 * a + b }
    })
    .filter(it => it.a < 100 && it.b < 100 && Number.isInteger(parseFloat(it.tokens.toFixed(8))))
    .map(it => it.tokens)
    .reduce((a, b) => a + b, 0)
)


