const fs = require('fs')

const [patternsStr, towelsStr] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')

const patterns = patternsStr.split(', ')
const towels = towelsStr.split('\n')

const cache = {}
const canBeMade = (towel) => {
  if (towel == '') return true
  if (cache[towel] == null) {
    cache[towel] = patterns
      .filter(p => towel.startsWith(p))
      .some(p => canBeMade(towel.substring(p.length)))
  }
  return cache[towel]
}

console.log(towels.filter(canBeMade).length)
