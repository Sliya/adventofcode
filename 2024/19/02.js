const fs = require('fs')

const [patternsStr, towelsStr] = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n\n')

const patterns = patternsStr.split(', ')
const towels = towelsStr.split('\n')

const cache = {}
const canBeMade = (towel) => {
  if (towel == '') return 1
  if (cache[towel] == null) {
    cache[towel] = patterns
      .filter(p => towel.startsWith(p))
      .map(p => canBeMade(towel.substring(p.length)))
      .reduce((a, b) => a + b, 0)
  }
  return cache[towel]
}
console.log(towels.map(canBeMade).reduce((a, b) => a + b, 0))
