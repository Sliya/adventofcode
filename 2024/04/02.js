const fs = require('fs')

const lines = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

function check(i, j) {
  if (i + 2 >= lines.length || j + 2 >= lines[0].length) return false
  const d1 = lines[i][j] + lines[i + 1][j + 1] + lines[i + 2][j + 2]
  const d2 = lines[i + 2][j] + lines[i + 1][j + 1] + lines[i][j + 2]
  return (d1 == "MAS" || d1 == "SAM") && (d2 == "MAS" || d2 == "SAM") ? 1 : 0
}

let count = 0
for (let i = 0; i < lines.length; i++) {
  for (let j = 0; j < lines[0].length; j++) {
    count += check(i, j)
  }
}

console.log(count)
