const fs = require('fs')

const lines = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split(''))

function check(i, j) {
  let c = 0
  if (i + 3 < lines.length) {
    const w = lines[i][j] + lines[i + 1][j] + lines[i + 2][j] + lines[i + 3][j]
    if (w == "XMAS" || w == "SAMX") c++
  }
  if (j + 3 < lines[0].length) {
    const w = lines[i][j] + lines[i][j + 1] + lines[i][j + 2] + lines[i][j + 3]
    if (w == "XMAS" || w == "SAMX") c++
  }
  if (i + 3 < lines.length && j + 3 < lines[0].length) {
    const w = lines[i][j] + lines[i + 1][j + 1] + lines[i + 2][j + 2] + lines[i + 3][j + 3]
    if (w == "XMAS" || w == "SAMX") c++
  }
  if (i - 3 >= 0 && j + 3 < lines[0].length) {
    const w = lines[i][j] + lines[i - 1][j + 1] + lines[i - 2][j + 2] + lines[i - 3][j + 3]
    if (w == "XMAS" || w == "SAMX") c++
  }
  return c
}

let count = 0
for (let i = 0; i < lines.length; i++) {
  for (let j = 0; j < lines[0].length; j++) {
    count += check(i, j)
  }
}

console.log(count)
