const fs = require('fs')

let connections = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split('-'))
  .reduce((acc, e) => {
    if (acc[e[0]] == null) acc[e[0]] = []
    if (acc[e[1]] == null) acc[e[1]] = []
    acc[e[0]].push(e[1])
    acc[e[1]].push(e[0])
    return acc
  }, {})

const nodes = [...Object.keys(connections)]

const dfs = (n, l = 0, dist = {}, p = [], prec = {}, visited = new Set()) => {
  if (visited.has(n)) return
  if (p.some(it => !connections[it].includes(n))) return
  visited.add(n)
  dist[n] = l
  prec[n] = p
  connections[n].forEach(nn => {
    dfs(nn, l + 1, dist, [...p, n], prec, visited)
  })
  return [dist, prec]
}

const longestCycle = (n, dist, prec) => {
  const farest = Object.entries(dist)
    .filter(it => connections[it[0]].includes(n))
    .reduce((acc, e) => acc[1] > e[1] ? acc : e, [null, -Infinity])
  [0]
  return [farest, ...prec[farest]].toSorted().join(',')

}

console.log(
  nodes.map(it => longestCycle(it, ...dfs(it)))
    .reduce((acc, e) => e.length > acc.length ? e : acc, { length: -Infinity })
)

