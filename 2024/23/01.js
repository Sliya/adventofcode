const fs = require('fs')

let connections = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => it.split('-'))
  .reduce((acc, e) => {
    if (acc[e[0]] == null) acc[e[0]] = []
    if (acc[e[1]] == null) acc[e[1]] = []
    acc[e[0]].push(e[1])
    acc[e[1]].push(e[0])
    return acc
  }, {})

const nodes = [...Object.keys(connections)]

let cycles = new Set()
nodes.forEach(n => {
  if (!n.startsWith('t')) return
  connections[n].forEach(nn => {
    if (nn == n) return
    connections[nn].forEach(nnn => {
      if (nnn == nn) return
      if (connections[nnn].includes(n)) cycles.add([n, nn, nnn].toSorted().join(''))
    })
  })
})

console.log(cycles.size)

