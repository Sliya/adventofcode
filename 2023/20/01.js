const fs = require('fs')

const count = { 'H': 0, 'L': 0 }
const queue = []

class Module {
  constructor(name) {
    this.name = name
    this.listeners = []
    this.sources = []
  }

  addListener(l) {
    this.listeners.push(l)
    l.addSource(this)
  }

  addSource(s) {
    this.sources.push(s)
  }

  receive(signal) {
    this.listeners.forEach(l => queue.push({ from: this, to: l, value: signal.value }))
  }
}

class FlipFlop extends Module {

  constructor(name) {
    super(name)
    this.on = false
  }

  receive(signal) {
    if (signal.value == 'L') {
      this.on = !this.on
      this.listeners.forEach(l => queue.push({ from: this, to: l, value: this.on ? 'H' : 'L' }))
    }
  }
}

class Conjunction extends Module {
  constructor(name) {
    super(name)
    this.last = {}
  }

  receive(signal) {
    const emitter = signal.from.name
    this.last[emitter] = signal.value
    const value = Object.values(this.last).every(it => it == 'H') ? 'L' : 'H'
    this.listeners.forEach(l => queue.push({ from: this, to: l, value }))
  }

  addSource(s) {
    super.addSource(s)
    this.last[s.name] = 'L'
  }

}

const modules = {}
let moduleDecl = fs.readFileSync('./input', {
  encoding: 'utf8',
  flag: 'r'
})
  .trim()
  .split('\n')
  .map(it => {
    let [source, dest] = it.split(' -> ')
    dest = dest.split(', ')
    if (source.startsWith('%')) {
      const name = source.substring(1)
      modules[name] = new FlipFlop(name)
      return { name, dest }
    } else if (source.startsWith('&')) {
      const name = source.substring(1)
      modules[name] = new Conjunction(name)
      return { name, dest }
    } else {
      modules[source] = new Module(source)
      return { name: source, dest }
    }
  })

moduleDecl.forEach(m => {
  m.dest.forEach(d => {
    if (modules[d] == null) {
      modules[d] = new Module(d)
    }
    modules[m.name].addListener(modules[d])
  })
})

for (let i = 0; i < 1000; i++) {
  queue.push({ from: { name: 'button' }, to: { name: 'broadcaster' }, value: 'L' })
  while (queue.length > 0) {
    const s = queue.shift()
    count[s.value]++
    modules[s.to.name].receive(s)
  }
}

console.log(count.H * count.L)
